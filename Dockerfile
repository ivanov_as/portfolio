FROM node:8.7.0

# The base node image sets a very verbose log level.
ENV NPM_CONFIG_LOGLEVEL warn

# Copy all local files into the image.
COPY . .

RUN npm install chalk
RUN npm install each-async
RUN npm install indent-string

# Build for production.
RUN npm run build --production

# Install `serve` to run the application.
RUN npm install -g serve

# Run serve when the image is run.
CMD serve -s build

# Let Docker know about the port that serve runs on.
EXPOSE 5000