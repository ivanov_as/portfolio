import {ArticleComment} from '../constants/action-types';
import {createAction} from 'redux-actions';

export const createArticleComment = createAction(ArticleComment.CREATE);

