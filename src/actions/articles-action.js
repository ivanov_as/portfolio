import {Article} from '../constants/action-types';
import {createAction} from 'redux-actions';

export const listArticle = (offset = 1, isClear = false) => ({
    types: [Article.FETCH, Article.FETCH_SUCCESS, Article.FETCH_FAILURE],
    payload: {
        request: {
            url: '/articles',
            params: {
                offset: offset
            }
        }
    },
    meta: {
        isClear
    }
});

export const selectArticle = createAction(Article.SELECTED);
