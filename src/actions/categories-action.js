import {Category} from '../constants/action-types';
import {createAction} from 'redux-actions';

export const listCategory = () => ({
    types: [Category.FETCH, Category.FETCH_SUCCESS, Category.FETCH_FAILURE],
    payload: {
        request: {
            url: '/categories'
        }
    }
});

export const selectCategory = createAction(Category.SELECTED);