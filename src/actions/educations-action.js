import {Education} from '../constants/action-types';
import {createAction} from 'redux-actions';

export const listEducation = createAction(Education.FETCH);
