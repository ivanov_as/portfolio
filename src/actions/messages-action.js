import {Message} from '../constants/action-types';
import {Map} from 'immutable';

export const createMessage = (message = Map()) => ({
    types: [Message.CREATE, Message.CREATE_SUCCESS, Message.CREATE_FAILURE],
    payload: {
        request: {
            method: 'POST',
            url: '/messages',
            data: message.toJSON()
        }
    }
});
