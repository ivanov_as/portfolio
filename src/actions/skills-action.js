import {Skill} from '../constants/action-types';

export const listSkill = () => ({
    types: [Skill.FETCH, Skill.FETCH_SUCCESS, Skill.FETCH_FAILURE],
    payload: {
        request: {
            url: '/skills'
        }
    }
});
