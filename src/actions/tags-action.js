import {Tag} from '../constants/action-types';

export const listTag = () => ({
    types: [Tag.FETCH, Tag.FETCH_SUCCESS, Tag.FETCH_FAILURE],
    payload: {
        request: {
            url: '/tags'
        }
    }
});

