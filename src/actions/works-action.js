import {Work} from '../constants/action-types';
import {createAction} from 'redux-actions';

export const listWork = (offset = 1, isClear = false) => ({
    types: [Work.FETCH, Work.FETCH_SUCCESS, Work.FETCH_FAILURE],
    payload: {
        request: {
            url: '/works',
            params: {
                offset: offset
            }
        }
    },
    meta: {
        isClear
    }
});

export const selectWork = createAction(Work.SELECTED);
