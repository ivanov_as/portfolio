import React from 'react';
import {Link, Image, Caption} from '../../index';
import moment from 'moment';
import {article} from '../../../constants';
import utils from '../../../utils';
import './styles.scss';

const ArticleRelatedItem = ({article}) => {

    return (
        <div className='article-related mg-right_sm mg-bm_sm'>
            <Image className='article-related__img' src={article.get('url')}/>
            <div className='article-related__content padding pd-right pd-left'>
                <Caption><span className='article-related__title'>{article.get('article_name')}</span></Caption>
                <div className='article-related__block'>
                    <span>{moment(article.get('article_d_public')).format('LL')}</span> / <span>{utils.splitAssociateListByField(article.get('articles_categories'), 'category', 'category_name')}</span>
                </div>
                <p className='article-related__desc mg-top_sm'>{article.get('article_desc')} </p>
                <Link className='kv-link-theme-light'>Read more</Link>
            </div>
        </div>
    );
};

ArticleRelatedItem.propTypes = {
    article: article.isRequired
};

export default ArticleRelatedItem;