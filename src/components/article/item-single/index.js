import React, {Component} from 'react';
import {Image, Link, Caption} from '../../index';
import {article} from '../../../constants';
import utils from '../../../utils';
import moment from 'moment';
import './styles.scss';


export default class ArticleSingleItem extends Component {

    static propTypes = {
        article: article.isRequired
    };

    renderShares = () => {
        return this.props.article.get('articles_shares').map((item) => (
            <Link className='kv-link-xlg kv-link-theme-light mg-left_sm' key={item.get('id')}><i className={`fa fa-${item.get('share').get('share_name')}`}
                                          aria-hidden='true'/></Link>))
    };

    render() {
        const {article} = this.props;
        return (
            <div className='article-single mg-bm_lg'>
                <div>
                    <Caption><h1 className='article-single__title'>{article.get('article_name')}</h1></Caption>
                    <div className='article-single__block margin'>
                        <span>{moment(article.get('article_d_public')).format('LL')}</span> / <span>Ivanov</span> / <span>{utils.splitAssociateListByField(article.get('articles_categories'), 'category', 'category_name')}</span>
                    </div>
                </div>
                <div>
                    <Image className='article-single__img mg-right mg-bm' src={article.get('article_url')}/>
                    <p className='article-single__desc'>{article.get('article_desc')}</p>
                    <div className='article-single__shares mg-top'>
                        <span>Shares:</span>
                        <div className='shares__links mg-top_xs'>
                            {this.renderShares()}
                        </div>
                    </div>
                </div>
            </div>
        );
    }
};
