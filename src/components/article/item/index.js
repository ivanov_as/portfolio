import React from 'react';
import moment from 'moment';
import {Image, Button, Caption} from '../../index';
import {article} from '../../../constants';
import './styles.scss';

const ArticleItem = ({article}) => {

    return (
        <div className='article mg-bm'>
            <Image className='article__img' src={article.get('article_url')}/>
            <div className='article__content padding-lg pd-left_lg pd-right_lg'>
                <Caption className='kv-caption-theme-light mg-bm'><h4>{article.get('article_name')}</h4></Caption>
                <span className='article__comment mg-bm_sm'>{moment(article.get('article_d_public')).format('LL')} | {article.get('articles_comments').size} Comments</span>
                <p className='article__desc mg-bm_lg'>{article.get('article_desc')}</p>
                <Button className='kv-btn-sm kv-btn-theme-light article__btn' href={`/blog/${article.get('id')}`}>more read</Button>
            </div>
        </div>
    );
};

ArticleItem.propTypes = {
    article: article.isRequired
};

export default ArticleItem;