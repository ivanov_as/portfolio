import React from 'react';
import PropTypes from 'prop-types';
import {Caption} from '../index';
import './styles.scss';

const BlockWithTitle = ({title = 'Unnamed', children}) => {
    return (
        <div className='block block-theme-br-bm'>
            <Caption className='pd-bm_sm kv-caption-theme-light'>
                <h4>{title}</h4>
            </Caption>
            <div className="block__content pd-top_xs">
                {children}
            </div>
        </div>
    );
};

BlockWithTitle.propTypes = {
    title: PropTypes.string.isRequired
};

export default BlockWithTitle;