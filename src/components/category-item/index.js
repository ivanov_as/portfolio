import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import {Link} from '../index';
import {category} from '../../constants';
import './styles.scss';

const CategoryItem = ({category, className = '', classEvents, onClick}) => {
    let categoryClass = classNames(`category ${className}`, classEvents);
    return (
        <div className={categoryClass}>
            <Link className='kv-link-lg kv-link-theme-light' onClick={(event) => {
                event.preventDefault();
                onClick(category)
            }}>{category.get('category_name')}</Link>
        </div>
    );
};

CategoryItem.propTypes = {
    category: category.isRequired,
    onClick: PropTypes.func,
    className: PropTypes.string,
    classEvents: PropTypes.object
};

export default CategoryItem;