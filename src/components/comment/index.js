import React from 'react';
import PropTypes from 'prop-types';
import ImmutablePropTypes from 'react-immutable-proptypes';
import {Image, BlockWithTitle} from '../index';
import './styles.scss';

const Comment = ({comment}) => {
    return (
        <div className="comment-wrapper">
            <div className="face-wrapper">
                <Image src={comment.get('src')}/>
            </div>
            <BlockWithTitle title={comment.get('name')}>
                <p>{comment.get('desc')}</p>
            </BlockWithTitle>
        </div>
    );
};

Comment.propTypes = {
    comment: ImmutablePropTypes.mapContains({
        id: PropTypes.number.isRequired,
        name: PropTypes.string.isRequired,
        src: PropTypes.string,
        desc: PropTypes.string.isRequired
    }).isRequired
};

export default Comment;