import React from 'react';
import PropTypes from 'prop-types';
import {Link} from 'react-router-dom';
import './styles.scss';

const Button = ({onClick, href, className = '', children}) => {

    if (href) {
        return (<Link to={href}>
            <Button className={`kv-btn ${className}`} onClick={onClick}>{children}</Button>
        </Link>)
    } else {
        return (
            <div className={`kv-btn ${className}`}>
                <button onClick={onClick}>{children}</button>
            </div>
        );
    }
};

Button.propTypes = {
    onClick: PropTypes.func
};

export default Button;