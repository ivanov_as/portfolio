import React from 'react';
import PropTypes from 'prop-types';
import './styles.scss';

const Caption = ({children, className = ''}) => {
    return (
        <div className={`kv-caption ${className}`}>
            {children}
        </div>
    );
};

Caption.propTypes = {
    className: PropTypes.string
};

export default Caption;