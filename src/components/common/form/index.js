import React from 'react';
import PropTypes from 'prop-types';
import './styles.scss';

const Form = ({children, className = ''}) => {
    return (
        <div className={`kv-form ${className}`}>
            {children}
        </div>
    );
};

Form.propTypes = {
    className: PropTypes.string
};

export default Form;