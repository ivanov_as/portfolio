import React from 'react';
import PropTypes from 'prop-types';
import './styles.scss';

const Image = ({src, alt, className = ''}) => {
    return (
        <div className={`kv-img ${className}`}>
            <img src={src} alt={alt}/>
        </div>
    );
};

Image.propTypes = {
    src: PropTypes.string.isRequired,
    className: PropTypes.string,
    alt: PropTypes.string
};

export default Image;