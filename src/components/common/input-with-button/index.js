import React, {Component} from 'react';
import {Map} from 'immutable';
import PropTypes from 'prop-types';
import {Button, Input} from '../../index';
import './styles.scss';

export default class InputWithButton extends Component {

    static propTypes = {
        caption: PropTypes.string.isRequired,
        placeholder: PropTypes.string.isRequired,
        type: PropTypes.string,
        className: PropTypes.string,
        onClick: PropTypes.func.isRequired
    };

    static defaultProps = {
        placeholder: '',
        caption: '',
        type: 'text'
    };

    state = {
        data: Map({
            value: ''
        })
    };

    changeInput = (e) => {
        let value = e.target.value;
        this.setState((state) => ({data: state.data.set('value', value)}));
    };

    render() {
        const {
            caption,
            placeholder,
            type,
            onClick,
            className
        } = this.props;

        return (
            <div className={`input-btn ${className || ''}`}>
                <Input className='kv-input-sm kv-input-theme-light input-btn__input' type={type} placeholder={placeholder} onChange={this.changeInput}/>
                <Button className='kv-btn-sm kv-btn-theme-light input-btn__btn' onClick={() => {
                    onClick(this.state.data.get('value'));
                }}>{caption}</Button>
            </div>
        );
    }
};
