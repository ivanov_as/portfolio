import React from 'react';
import PropTypes from 'prop-types';
import './styles.scss';

const Input = ({type, placeholder, onChange, className = ''}) => {
    return (
        <div className={`kv-input ${className}`}>
            <input type={type} placeholder={placeholder} onChange={onChange}/>
        </div>
    );
};

Input.propTypes = {
    type: PropTypes.string.isRequired,
    placeholder: PropTypes.string,
    className: PropTypes.string,
    onChange: PropTypes.func
};

export default Input;