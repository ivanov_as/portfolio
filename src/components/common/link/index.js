import React from 'react';
import PropTypes from 'prop-types';
import './styles.scss';

const Link = ({onClick, children, className}) => {
    return (
        <div className={`kv-link ${className}`}>
            <span onClick={onClick}>{children}</span>
        </div>
    );
};

Link.propTypes = {
    onClick: PropTypes.func,
    className: PropTypes.string
};

export default Link;