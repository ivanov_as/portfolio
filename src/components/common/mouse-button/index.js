import React from 'react';
import PropTypes from 'prop-types';
import './styles.scss';

const MouseBtn = ({className = ''}) => {
    return (
        <div className={`mouse-button ${className}`}/>
    );
};

MouseBtn.propTypes = {
    onClick: PropTypes.func
};

export default MouseBtn;