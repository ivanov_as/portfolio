import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import {Container} from 'reactstrap';
import './styles.scss';

const PageContainer = ({className, id, fluid, children}) => {
    let pageClass = classNames("page-wrapper", className);
    return (
        <Container id={id} fluid={fluid} className={pageClass}>
            {children}
        </Container>
    );
};

PageContainer.defaultProps = {
    fluid: false,
    className: ""
};

PageContainer.propTypes = {
    fluid: PropTypes.bool,
    id: PropTypes.string,
    className: PropTypes.string
};

export default PageContainer;