import React from 'react';
import PropTypes from 'prop-types';
import './styles.scss';

const Text = ({placeholder, onChange, className = ''}) => {
    return (
        <div className={`kv-text ${className}`}>
            <textarea rows={5} placeholder={placeholder} onChange={onChange}/>
        </div>
    );
};

Text.propTypes = {
    placeholder: PropTypes.string,
    onChange: PropTypes.func,
    className: PropTypes.string
};

export default Text;