import React from 'react';
import PropTypes from 'prop-types';
import './styles.scss';

const ToolBar = ({children, className = ''}) => {
    return (
        <div className={`kv-toolbar ${className}`}>
            {children}
        </div>
    );
};

ToolBar.propTypes = {
    className: PropTypes.string
};

export default ToolBar;