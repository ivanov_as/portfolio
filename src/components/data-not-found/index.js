import React from 'react';
import PropTypes from 'prop-types';
import './styles.scss';

const DataNotFound = ({className = ''}) => {
    return (
        <div className={`data-not-found ${className}`}>
            <span className='padding'>Данные не найдены</span>
        </div>
    );
};

DataNotFound.propTypes = {
    className: PropTypes.string
};

export default DataNotFound;