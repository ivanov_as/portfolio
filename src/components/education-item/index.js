import React from 'react';
import PropTypes from 'prop-types';
import ImmutablePropTypes from 'react-immutable-proptypes';
import {Col, Row} from 'reactstrap';
import './styles.scss';

const EducationItem = ({education, className = ''}) => {
    return (
        <Row className={`education ${className}`}>
            <div className="item">
                <div className="line"/>
                <div className="point"/>
            </div>
            <Col md={6} className="education__header pd-left_lg pd-right_lg">
                <h4>{education.get('institute')}</h4>
                <span>Факультет <span>{education.get('pulpit')}</span> - <span>{education.get('year')}</span></span>
            </Col>
            <Col md={6} className="education__content pd-left_lg pd-right_lg">
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus laoreet dolor metus, eu ullamcorper
                    turpis ornare tincidunt. Vivamus tristique rhoncus enim.</p>
            </Col>
        </Row>
    );
};

EducationItem.propTypes = {
    education: ImmutablePropTypes.mapContains({
        id: PropTypes.number.isRequired,
        institute: PropTypes.string.isRequired,
        pulpit: PropTypes.string,
        year: PropTypes.number.isRequired
    }).isRequired,
    className: PropTypes.string
};

export default EducationItem;