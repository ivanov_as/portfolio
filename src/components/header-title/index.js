import React from 'react';
import PropTypes from 'prop-types';
import {Container} from 'reactstrap';
import {Caption} from '../index';
import './styles.scss';

const HeaderTitle = ({caption = '', description = ''}) => {
    return (
        <div className='header-caption'>
            <Container className='header-caption__content padding-xlg pd-left pd-right'>
                <div className="header-caption__name">
                    <Caption className='kv-caption-theme-light mg-bm_sm'>
                        <h1>{caption}</h1>
                    </Caption>
                    <span className='header-caption__desc'>{description}</span>
                </div>
                <div className="header-caption__back"/>
                <div className="header-caption__mask"/>
            </Container>
        </div>
    );
};

HeaderTitle.propTypes = {
    caption: PropTypes.string.isRequired,
    description: PropTypes.string.isRequired
};

export default HeaderTitle;