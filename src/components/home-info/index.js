import React from 'react';
import {Container} from 'reactstrap';
import './styles.scss';

const HomeInfo = () => {
    return (
        <div className="home-info">
            <iframe className='home-info__video' title="back" allowFullScreen={true}
                    src="https://www.youtube.com/embed/BpYcr80J3QU?modestbranding=0 q&autoplay=1&controls=0&showinfo=0&loop=1&playlist=BpYcr80J3QU"
                    frameBorder={0}/>
            <div className="home-info__back"/>
            <Container className="home-info__content">
                <h1 className='mg-bm_lg'>Hello!</h1>
                <h2 className='mg-bm'>I'm Ivanov Aleksandr</h2>
                <h3>Web Developer & Web Designer</h3>
            </Container>
        </div>
    );
};

HomeInfo.propTypes = {};

export default HomeInfo;