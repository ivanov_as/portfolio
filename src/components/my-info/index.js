import React from 'react';
import {Caption, Link} from '../index';
import './styles.scss';

const MyInfo = () => {
    return (
        <div className='my-info pd-top_lg'>
            <div className='my-info__about-me'>
                <Caption className='mg-bm kv-caption-theme-light'><h1>about me</h1></Caption>
                <p>Hello! I’m Aleksandr Ivanov. Web Developer with over 6 years of experience. Experienced with all stages
                    of the development cycle for dynamic web projects. Having an in-depth knowledge including advanced
                    HTML5, CSS3, JavaScript, jQuery, React JS. Strong background in management and leadership.</p>
            </div>
            <div className="my-info__line margin">
                <Caption className='kv-caption-theme-light'><h4>NAME:</h4></Caption>
                <span className='pd-left_sm'>Aleksandr Ivanov</span>
            </div>
            <div className="my-info__line margin">
                <Caption className='kv-caption-theme-light'><h4>DATE OF BIRTH:</h4></Caption>
                <span className='pd-left_sm'>17 Октября 1989</span>
            </div>
            <div className="my-info__line margin">
                <Caption className='kv-caption-theme-light'><h4>PHONE:</h4></Caption>
                <span className='pd-left_sm'>(931)-287-1073</span>
            </div>
            <div className="my-info__line margin">
                <Caption className='kv-caption-theme-light'><h4>EMAIL:</h4></Caption>
                <Link className='kv-link-lg pd-left_sm kv-link-theme-light' href='mailto:red.systems@yandex.ru'>red.systems@yandex.ru</Link>
            </div>
        </div>
    );
};

export default MyInfo;
