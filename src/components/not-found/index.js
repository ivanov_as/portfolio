import React from 'react';
import {Container} from 'reactstrap';
import {Link} from '../index';
import './styles.scss';

const NotFound = () => {
    return (
        <div className="not-found">
            <Container>
                <h1>404 <h4>Page not found</h4></h1>
                <Link>Перейти на главную страницу</Link>
            </Container>
        </div>
    );
};

export default NotFound;