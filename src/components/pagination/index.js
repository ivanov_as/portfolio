import React, {Component} from 'react';
import {Link, Button} from '../index';
import {Map} from 'immutable';
import PropTypes from 'prop-types';
import './styles.scss';

export default class Pagination extends Component {

    static propTypes = {
        limit: PropTypes.number,
        size: PropTypes.number,
        onClick: PropTypes.func
    };

    static defaultProps = {
        limit: 5,
        size: 0
    };

    state = {
        data: Map({
            countBtnMax: 5,
            index: 0,
            activeIndex: 0
        })
    };

    clickBtnList = (event) => {
        const {onClick} = this.props;
        let offset = Number(event.target.innerHTML) - 1;
        this.setState((state) => ({data: state.data.set('activeIndex', offset)}));
        if (onClick) onClick(offset);
    };

    priorBtnClick = () => {
        this.setState((state) => ({data: state.data.set('index', state.data.get('index') - 1)}));
    };

    nextBtnClick = () => {
        this.setState((state) => ({data: state.data.set('index', state.data.get('index') + 1)}));
    };

    renderItems = () => {

        const {
            limit,
            size
        } = this.props;

        const countBtnMax = this.state.data.get('countBtnMax'),
            activeIndex = this.state.data.get('activeIndex'),
            index = this.state.data.get('index');

        let count = Math.floor(size / limit);
        let values = [];

        for (let idx = 1; idx <= countBtnMax; idx++) {
            let step = idx + limit * index;
            if (count >= step) values.push(step);
        }

        return values.map((item, idx) => {
            let active = activeIndex === idx + limit * index ? 'active' : '';
            return (<Link className={`kv-link-theme-btn-dark mg-left_xs mg-right_xs ${active}`} key={idx} onClick={this.clickBtnList}>{item}</Link>)
        });
    };

    renderBtnPrior = () => {
        const {size, limit} = this.props;
        if (size > limit && this.state.data.get('index')) {
            return (<Button className='kv-btn-theme-light mg-right_xs' onClick={this.priorBtnClick}>Prior</Button>);
        }
    };

    renderBtnNext = () => {
        const {size, limit} = this.props;
        let count = Math.floor(Math.floor(size / limit) / limit);

        if (size > limit && this.state.data.get('index') < count) {
            return (<Button className='kv-btn-theme-light mg-left_xs' onClick={this.nextBtnClick}>Next</Button>);
        }
    };

    render() {
        return (
            <div className='pagination'>
                {this.renderBtnPrior()}
                {this.renderItems()}
                {this.renderBtnNext()}
            </div>
        );
    }
};