import React from 'react';
import PropTypes from 'prop-types';
import {Row, Col} from 'reactstrap';
import {skill} from '../../constants';
import './styles.scss';

const SkillItem = ({skill, className = ''}) => {
    return (
        <Row className={`skill ${className}`}>
            <div className="item">
                <div className="line"/>
                <div className="point"/>
            </div>
            <Col md={6} className="skill__header pd-left_lg pd-right_lg">
                <h4>{skill.get('skill_name')}</h4>
            </Col>
            <Col md={6} className="skill__content pd-left_lg pd-right_lg">
                <p>{skill.get('skill_desc')}</p>
            </Col>
        </Row>
    );
};

SkillItem.propTypes = {
    skill: skill.isRequired,
    className: PropTypes.string
};

export default SkillItem;