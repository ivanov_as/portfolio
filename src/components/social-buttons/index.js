import React from 'react';
import PropTypes from 'prop-types';
import {Link} from '../index';
import './styles.scss';

const SocialButtons = ({className = ''}) => {
    return (
        <div className={`social-buttons ${className}`}>
            <Link className='kv-link-lg social-buttons__link'><i className="fa fa-twitter" aria-hidden="true"/></Link>
            <Link className='kv-link-lg social-buttons__link'><i className="fa fa-github" aria-hidden="true"/></Link>
            <Link className='kv-link-lg social-buttons__link'><i className="fa fa-google-plus" aria-hidden="true"/></Link>
            <Link className='kv-link-lg social-buttons__link'><i className="fa fa-linkedin" aria-hidden="true"/></Link>
        </div>
    );
};

SocialButtons.propTypes = {
  className: PropTypes.string
};

export default SocialButtons;