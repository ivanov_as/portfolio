import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import {Link} from '../index';
import {tag} from '../../constants';
import './styles.scss';


const TagItem = ({tag, className, onClick}) => {
    let tagClass = classNames("tag mg-bm_xs mg-right_xs", className || {});
    return (
        <div className={tagClass}>
            <Link className='kv-link-theme-btn-dark' onClick={(event) => {
                event.preventDefault();
                if (onClick) onClick(tag)
            }}>{tag.get('tag_name')}</Link>
        </div>
    );
};

TagItem.propTypes = {
    tag: tag.isRequired,
    onClick: PropTypes.func,
    className: PropTypes.string
};

export default TagItem;