import React, {Component} from 'react';
import {Image, Link, DataNotFound, Caption, ToolBar} from '../../index';
import {Container, Col} from 'reactstrap';
import Utils from '../../../utils';
import {work} from '../../../constants';
import './styles.scss';

export default class WorkDetail extends Component {

    static propTypes = {
        work: work.isRequired
    };

    renderShare = () => {
        return this.props.work.get('works_shares').map((item) => (
            <Link className='kv-link-xlg kv-link-theme-light mg-top_sm mg-right_sm' key={item.get('id')}>
                <i className={`fa fa-${item.get('share').get('share_name')}`} aria-hidden="true"/>
            </Link>
        ));
    };

    render() {
        const {work} = this.props;

        if (work) {

            const skills = Utils.splitAssociateListByField(work.get('works_skills'), 'skill', 'skill_name');

            return (
                <Container className='work-detail'>
                    <Col md={{size: 8}} className='pd-top_lg'>
                        <Image className='work-detail__img' src={work.get('work_url')} alt=""/>
                    </Col>
                    <Col md={4} className='pd-top_lg'>
                        <div className='work-detail__content'>
                            <Caption className='kv-caption-theme-light'><h5>project info</h5></Caption>
                            <p className='work-detail__desc margin'>{work.get('work_desc')}</p>
                            <Caption className='kv-caption-theme-light'><h5>client</h5></Caption>
                            <span className="work-detail__client margin">{work.get('client').get('client_name')}</span>
                            <Caption className='kv-caption-theme-light'><h5>skill used</h5></Caption>
                            <span className="work-detail__skills margin">{skills}</span>
                            <Caption className='kv-caption-theme-light'><h5>url</h5></Caption>
                            <Link className='kv-link-theme-light margin'>{work.get('work_site_url')}</Link>
                            <Caption className='kv-caption-theme-light'><h5>shares</h5></Caption>
                            <ToolBar className='work-detail__toolbar'>{this.renderShare()}</ToolBar>
                        </div>
                    </Col>
                </Container>
            );
        } else {
            return (
                <Container className='pd-top_lg'>
                    <DataNotFound className='data-not-found-theme-light'/>
                </Container>
            )
        }
    }
};
