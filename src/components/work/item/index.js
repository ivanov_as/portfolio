import React from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';
import {Link} from 'react-router-dom';
import {Image} from '../../index';
import {work} from '../../../constants';
import './styles.scss';

const WorkItem = ({work, selectWork, className = ''}) => {
    return (
        <div className={`work ${className}`}>
            <Image className='work__img' src={work.get('work_url')} alt=''/>
            <div className='work__back'/>
            <div className='work__content'>
                <Link className='kv-link work__name' to={`/work/${work.get('id')}`} onClick={selectWork(work)}>{work.get('work_name')}</Link>
                <span className='work__public'>{moment(work.get('work_d_public')).format('LL')}</span>
            </div>
        </div>
    );
};

WorkItem.propTypes = {
    work: work.isRequired,
    selectWork: PropTypes.func.isRequired,
    className: PropTypes.string
};

export default WorkItem;