import ImmutablePropTypes from 'react-immutable-proptypes';
import PropTypes from 'prop-types';

export const STYLE = {
    ACTIVE: 'active',
    FIXED: 'fixed',
    HIDDEN: 'hidden'
};

export const REMOTE_REQUEST = {remote: true};

export const skill = ImmutablePropTypes.mapContains({
    skill_name: PropTypes.string.isRequired,
    skill_desc: PropTypes.string,
    skill_percent: PropTypes.string.isRequired,
});

export const share = ImmutablePropTypes.mapContains({
    share_name: PropTypes.string.isRequired
});

export const category = ImmutablePropTypes.mapContains({
    category_name: PropTypes.string.isRequired
});

export const tag = ImmutablePropTypes.mapContains({
    tag_name: PropTypes.string.isRequired
});

export const client = ImmutablePropTypes.mapContains({
    client_name: PropTypes.string.isRequired
});

export const work = ImmutablePropTypes.mapContains({
    id: PropTypes.number.isRequired,
    work_name: PropTypes.string.isRequired,
    work_desc: PropTypes.string,
    client: client.isRequired,
    work_url: PropTypes.string.isRequired,
    work_d_public: PropTypes.string.isRequired,
    work_site_url: PropTypes.string.isRequired,
    works_tags: ImmutablePropTypes.listOf(ImmutablePropTypes.mapContains({
        tag: tag
    })),
    works_skills: ImmutablePropTypes.listOf(ImmutablePropTypes.mapContains({
        skill: skill
    })),
    works_shares: ImmutablePropTypes.listOf(ImmutablePropTypes.mapContains({
        share: share
    })),
    works_categories: ImmutablePropTypes.listOf(ImmutablePropTypes.mapContains({
        category: category
    }))
});

export const article = ImmutablePropTypes.mapContains({
    article_name: PropTypes.string.isRequired,
    article_desc: PropTypes.string.isRequired,
    article_url: PropTypes.string,
    article_d_public: PropTypes.string.isRequired,
    articles_tags: ImmutablePropTypes.listOf(ImmutablePropTypes.mapContains({
        tag: tag
    })),
    articles_categories: ImmutablePropTypes.listOf(ImmutablePropTypes.mapContains({
        category: category
    })),
    articles_shares: ImmutablePropTypes.listOf(ImmutablePropTypes.mapContains({
        share: share
    }))
});
