import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {Col, Row, Container} from 'reactstrap';
import {PageContainer, Image, MyInfo, Button} from '../../components/index';
import {EducationsList, SkillsList} from '../index';
import './styles.scss';

export default class About extends Component {

    static displayName = 'About';

    static defaultProps = {
        fluid: false
    };

    static propTypes = {
        fluid: PropTypes.bool
    };

    componentDidMount() {
        document.title = "About";
    }

    render() {
        const {fluid} = this.props;
        return (
            <PageContainer id="about" fluid={fluid} className="about-page">
                <Container className='pd-bm_lg'>
                    <Row>
                        <Col md={6}>
                            <div className="about-page__left pd-top_lg">
                                <Image className='about-page__img' src={""}/>
                                <Button className='kv-btn_center mg-top_lg kv-btn-theme-light'>download resume</Button>
                            </div>
                        </Col>
                        <Col md={6}>
                            <MyInfo/>
                        </Col>
                    </Row>
                </Container>
                <EducationsList fluid={true}/>
                <SkillsList fluid={true}/>
            </PageContainer>
        );
    }
}
