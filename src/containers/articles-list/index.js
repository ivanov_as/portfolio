import React, {Component} from 'react';
import {connect} from 'react-redux';
import PropTypes from 'prop-types';
import ImmutablePropTypes from 'react-immutable-proptypes';
import {Container} from 'reactstrap';
import {ArticleItem, Button, Pagination, ToolBar, DataNotFound} from '../../components/index';
import {article} from '../../constants';
import {bindActionCreators} from 'redux';
import {listArticle} from '../../actions/articles-action';
import {dataSet} from '../../hoc/dataset';
import './styles.scss';

@dataSet()
@connect(mapStateToProps, mapDispatchToProps)
export default class RecentArticlesList extends Component {

    static displayName = 'RecentArticlesList';

    static defaultProps = {
        isBtnLoad: false,
        isPagination: false
    };

    static propTypes = {
        isBtnLoad: PropTypes.bool,
        isPagination: PropTypes.bool,
        articles: ImmutablePropTypes.listOf(article)
    };

    componentDidMount() {
        this.props.listArticle();
    }

    componentWillReceiveProps(newProps) {
        this.setState(newProps.articles);
    };

    get articles() {
        return this.props.articles;
    }

    clickBtnPagination = (offset) => {
        const {listArticle, dsNextPage} = this.props;
        dsNextPage(offset, listArticle);
    };

    clickBtnMore = () => {
        const {listArticle, dsNextPage} = this.props;
        dsNextPage(undefined, listArticle);
    };

    renderButtons = () => {
        const {
            isPagination,
            isBtnLoad,
            count
        } = this.props;
        return (
            <ToolBar className='kv-toolbar_center padding-lg'>
                {isBtnLoad ? (<Button className='kv-btn-lg kv-btn-theme-dark' onClick={this.clickBtnMore}>more load</Button>) : null}
                {isPagination ? (<Pagination limit={5} size={count} onClick={this.clickBtnPagination}/>) : null}
            </ToolBar>)
    };

    render() {

        if (this.articles.size) {
            return (
                <div className='articles'>
                    <div className='articles__items'>
                        {this.articles.map((item) => {
                            return (<ArticleItem key={item.get('id')} article={item}/>);
                        })}
                    </div>
                    {this.renderButtons()}
                </div>
            )
        } else {
            return (
                <Container>
                    <DataNotFound className='data-not-found-theme-light pd-bm_lg'/>
                </Container>)
        }
    }
}

function mapStateToProps(state) {
    return {
        articles: state.get('articles').get('list').get('rows'),
        count: state.get('articles').get('list').get('count'),
        category_active: state.get('categories').get('active')
    }
}

function mapDispatchToProps(dispatch) {
    return {
        listArticle: bindActionCreators(listArticle, dispatch)
    }
}
