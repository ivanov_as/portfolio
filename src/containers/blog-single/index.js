import React, {Component} from 'react';
import {connect} from 'react-redux';
import {Col, Row, Container} from 'reactstrap';
import {PageContainer, HeaderTitle, InputWithButton, ArticleSingleItem, DataNotFound, Caption} from '../../components/index';
import {RelatedArticlesList, CategoriesList, TagsList} from '../index';
import './styles.scss';

@connect(mapStateToProps, mapDispatchToProps)
export default class BlogSingle extends Component {

    static displayName = 'BlogSingle';

    clickFilterBtn = () => {

    };

    renderDetail = () => {

        const {blog} = this.props;

        if (blog) {
            return (<Container className='pd-top_lg'>
                <Row>
                    <Col md={9}>
                        <ArticleSingleItem article={blog}/>
                    </Col>
                    <Col md={3}>
                        <InputWithButton className='mg-bm_lg' caption="Search" placeholder="enter кeyword..." onClick={this.clickFilterBtn}/>
                        <CategoriesList className='mg-bm_lg'/>
                        <TagsList className='mg-bm_lg'/>
                    </Col>
                </Row>
                <Row>
                    <Col md={12}>
                        <Caption className='kv-caption-theme-light padding-lg'><h4>related posts</h4></Caption>
                        <RelatedArticlesList/>
                    </Col>
                </Row>
                {/*<Row>*/}
                    {/*<Col md={7}>*/}
                        {/*<CommentsList/>*/}
                    {/*</Col>*/}
                {/*</Row>*/}
            </Container>)
        } else {
            return (
                <Container>
                    <DataNotFound className='data-not-found-theme-light pd-top_lg'/>
                </Container>)

        }
    };


    render() {
        return (
            <PageContainer fluid={true} id="blog-single" className="blog-single">
                <HeaderTitle caption="blog"
                             description="Lorem ipsum dolor sit amet, consectetur eiusmod tempor incididunt ut labore et dolore magna aliqua."/>
                {this.renderDetail()}
            </PageContainer>
        );
    }
}

function mapStateToProps(state, ownProps) {
    return {
        blog: state.get('articles').get('list').get('rows').find((item) => (item.get('id') === Number(ownProps.match.params.id)))
    }
}

function mapDispatchToProps(dispatch) {
    return {}
}
