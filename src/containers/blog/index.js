import React, {Component} from 'react';
import {Map} from 'immutable';
import {Col, Row, Container} from 'reactstrap';
import {PageContainer, InputWithButton, HeaderTitle} from '../../components/index';
import {RecentArticlesList, CategoriesList, TagsList} from '../index';
import './styles.scss';

export default class Blog extends Component {

    static displayName = 'Blog';

    state = {
        searches: Map({
            article_key_word: ''
        })
    };

    componentDidMount() {
        document.title = "Blog";
    }

    clickBtnKeyWord = (word = '') => {
        this.setState((state) => ({searches: state.searches.set('article_key_word', word)}));
    };

    render() {
        return (
            <PageContainer id="blog" fluid={true} className="blog-page">
                <HeaderTitle caption="blog"
                             description="Lorem ipsum dolor sit amet, consectetur eiusmod tempor incididunt ut labore et dolore magna aliqua."/>
                <Container>
                    <Row className='pd-top_lg'>
                        <Col md={9} className="articles_theme">
                            <RecentArticlesList isPagination={true} search_key_word={this.state.searches.get('article_key_word')}/>
                        </Col>
                        <Col md={3}>
                            <InputWithButton className='mg-bm_lg' caption="Search" placeholder="enter кeyword..." onClick={this.clickBtnKeyWord}/>
                            <CategoriesList className='mg-bm_lg'/>
                            <TagsList className='mg-bm_lg'/>
                        </Col>
                    </Row>
                </Container>
            </PageContainer>
        );
    }
}
