import React, {Component} from 'react';
import {Map} from 'immutable';
import classNames from 'classnames';
import './styles.scss';

export default class ButtonUp extends Component {

    static displayName = 'ButtonUp';

    state = {
        data: Map({
            isUp: true
        })
    };

    componentDidMount() {
        document.addEventListener('scroll', this.onScroll, false);
    }

    componentWillUnmount() {
        document.removeEventListener('scroll', this.onScroll, false);
    }

    onScroll = () => {
        this.setState(state => ({data: state.data.set('isUp', document.documentElement.scrollTop <= 500)}));
    };

    render() {
        let btnClass = classNames("kv-btn-up", {
            "hidden": this.state.data.get('isUp')
        });
        return (
            <div className={btnClass}>
                <button onClick={() => {
                    window.scrollTo(0, 0);
                }}><i className="fa fa-angle-up" aria-hidden="true"/></button>
            </div>
        );
    }
}
