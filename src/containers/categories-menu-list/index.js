import React, {Component} from 'react';
import {connect} from 'react-redux';
import ImmutablePropTypes from 'react-immutable-proptypes';
import PropTypes from 'prop-types';
import {CategoryItem} from '../../components/index';
import {bindActionCreators} from 'redux';
import {listCategory, selectCategory} from '../../actions/categories-action';
import {category} from '../../constants';
import './styles.scss';

@connect(mapStateToProps, mapDispatchToProps)
export default class CategoriesMenuList extends Component {

    static displayName = 'CategoriesMenuList';

    static propTypes = {
        categories: ImmutablePropTypes.listOf(category),
        className: PropTypes.string
    };

    state = {
        activeIndex: 1
    };

    componentDidMount() {
        this.props.listCategory();
    }

    componentWillReceiveProps(newProps) {
        this.setState(newProps.categories);
    }

    render() {
        const {
            categories,
            selectCategory,
            className
        } = this.props;

        return (
            <div className={`categories-menu ${className}`}>
                {categories.map((item) => {
                    return (<CategoryItem className={'mg-bm_sm'} classEvents={this.state.activeIndex === item.get('id') ? 'active' : ''}
                                          key={item.get('id')} category={item} onClick={() => {
                        this.setState({activeIndex: item.get('id')});
                        selectCategory(item);
                    }}/>);
                })}
            </div>
        )
    }
}

function mapStateToProps(state) {
    return {
        categories: state.get('categories').get('list')
    }
}

function mapDispatchToProps(dispatch) {
    return {
        selectCategory: bindActionCreators(selectCategory, dispatch),
        listCategory: bindActionCreators(listCategory, dispatch)
    }
}
