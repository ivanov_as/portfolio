import React, {Component} from 'react';
import {connect} from 'react-redux';
import PropTypes from 'prop-types';
import {Map} from 'immutable';
import ImmutablePropTypes from 'react-immutable-proptypes';
import {bindActionCreators} from 'redux';
import {Row, Col} from 'reactstrap';
import {Comment, BlockWithTitle, Input, Form, Text, Button, Caption, ToolBar} from '../../components/index';
import {createArticleComment} from '../../actions/article-comment-action';
import './styles.scss';

@connect(mapStateToProps, mapDispatchToProps)
export default class CommentsList extends Component {

    static displayName = 'CommentsList';

    static propTypes = {
        comments: ImmutablePropTypes.listOf(ImmutablePropTypes.mapContains({
            id: PropTypes.number.isRequired,
            src: PropTypes.string.isRequired,
            name: PropTypes.string.isRequired,
            desc: PropTypes.string.isRequired
        }))
    };

    state = {
        comment: Map({
            name: '',
            mail: '',
            comment: ''
        })
    };

    componentWillReceiveProps(newProps) {
        this.setState(newProps.comments);
    }

    changeInputName = (e) => {
        let value = e.target.value;
        this.setState(state => ({comment: state.comment.set('name', value)}));
    };

    changeInputEmail = (e) => {
        let value = e.target.value;
        this.setState(state => ({comment: state.comment.set('mail', value)}));
    };

    changeInputComment = (e) => {
        let value = e.target.value;
        this.setState(state => ({comment: state.comment.set('comment', value)}));
    };

    render() {
        const {
            comments,
            createBlog
        } = this.props;

        const {
            comment
        } = this.state;

        return (
            <div className="comments-wrapper">
                <BlockWithTitle title={`Comments (${comments.count()})`}>
                    {comments.map((item) => {
                        return (<Comment key={item.get('id')} comment={item}/>);
                    })}
                </BlockWithTitle>
                <div className="comment-form">
                    <Caption className='kv-caption-theme-light'>
                        <h4>Leave a Comment</h4>
                    </Caption>
                    <Form>
                        <Row>
                            <Col md={6}>
                                <Input type="text" placeholder="EMAIL*" onChange={this.changeInputEmail}/>
                            </Col>
                            <Col md={6}>
                                <Input type="text" placeholder="NAME" onChange={this.changeInputName}/>
                            </Col>
                        </Row>
                        <Row>
                            <Col>
                                <Text placeholder="COMMENT" onChange={this.changeInputComment}/>
                            </Col>
                        </Row>
                        <ToolBar>
                            <Button onClick={() => {
                                createBlog(comment)
                            }}>send comment</Button>
                        </ToolBar>
                    </Form>
                </div>
            </div>
        )
    }
}

function mapStateToProps(state) {
    return {
        comments: state.get('comments').get('list')
    }
}

function mapDispatchToProps(dispatch) {
    return {
        createBlog: bindActionCreators(createArticleComment, dispatch)
    }
}
