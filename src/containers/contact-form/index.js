import React, {Component} from 'react';
import {connect} from 'react-redux';
import {Row, Col} from 'reactstrap'
import {Form, Input, Button, Text} from '../../components/index';
import {form} from '../../hoc/form';
import {createMessage} from '../../actions/messages-action';
import {bindActionCreators} from 'redux';
import './styles.scss';

@form()
@connect(mapStateToProps, mapDispatchToProps)
export default class ContactForm extends Component {

    static displayName = 'ContactForm';

    changeInputName = (e) => {
        let value = e.target.value;
        this.props.setData('message_name', value);
    };

    changeInputEmail = (e) => {
        let value = e.target.value;
        this.props.setData('message_email', value);
    };

    changeInputMessage = (e) => {
        let value = e.target.value;
        this.props.setData('message_body', value);
    };

    clickSubmit = () => {
        this.props.submitData((message) => {
            this.props.createMessage(message);
        });
    };

    render() {
        return (
            <Form className="contact-form pd-top_lg">
                <Row>
                    <Col md={6}>
                        <Input className='kv-input-theme-dark mg_bm' type="email" placeholder="email *" onChange={this.changeInputEmail}/>
                    </Col>
                    <Col md={6}>
                        <Input className='kv-input-theme-dark mg-bottom' type="text" placeholder="name" onChange={this.changeInputName}/>
                    </Col>
                </Row>
                <Row>
                    <Col>
                        <Text className='kv-text-theme-dark' placeholder="post your message" onChange={this.changeInputMessage}/>
                    </Col>
                </Row>
                <Button className='kv-btn-lg kv-btn_center kv-btn-theme-dark padding-lg' onClick={this.clickSubmit}>send message</Button>
            </Form>
        );
    }
}


function mapStateToProps() {
    return {}

}

function mapDispatchToProps(dispatch) {
    return {
        createMessage: bindActionCreators(createMessage, dispatch)
    }
}
