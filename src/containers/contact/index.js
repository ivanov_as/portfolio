import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {Col} from 'reactstrap';
import {PageContainer, Caption} from '../../components/index';
import {ContactForm} from '../index';
import './styles.scss';

export default class Contact extends Component {

    static displayName = 'Contact';

    static defaultProps = {
        fluid: false
    };

    static propTypes = {
        fluid: PropTypes.bool
    };

    componentDidMount() {
        document.title = "Contact";
    }

    render() {
        const {fluid} = this.props;
        return (
            <PageContainer id="contact" fluid={fluid} className="contact-page">
                <Col md={{size: 8, offset: 2}}>
                    <Caption className='kv-caption_center padding-lg kv-caption-theme-light'><h1>contact me</h1></Caption>
                    <ContactForm/>
                </Col>
            </PageContainer>
        );
    }
}
