import React, {Component} from 'react';
import {connect} from 'react-redux';
import PropTypes from 'prop-types';
import ImmutablePropTypes from 'react-immutable-proptypes';
import {Container} from 'reactstrap';
import {EducationItem, Caption} from '../../components/index';
import './styles.scss';

@connect(mapStateToProps, mapDispatchToProps)
export default class EducationsList extends Component {

    static displayName = 'EducationsList';

    static defaultProps = {
        fluid: false
    };

    static propTypes = {
        fluid: PropTypes.bool,
        educations: ImmutablePropTypes.listOf(ImmutablePropTypes.mapContains({
            id: PropTypes.number.isRequired,
            institute: PropTypes.string.isRequired,
            pulpit: PropTypes.string,
            year: PropTypes.number.isRequired
        }))
    };

    componentWillReceiveProps(newProps) {
        this.setState(newProps.educations);
    }

    render() {
        const {
            educations,
            fluid
        } = this.props;

        return (
            <Container fluid={fluid} className="educations pd-bm_lg">
                <Container>
                    <Caption className='kv-caption_center padding-lg kv-caption-theme-light'><h1>Educations</h1></Caption>
                    <div className="header-icon"><i className="fa fa-diamond" aria-hidden="true"/></div>
                    {educations.map((item, idx) => (<EducationItem className='mg-bm_lg' key={idx} education={item}/>))}
                </Container>
            </Container>
        )
    }
}

function mapStateToProps(state) {
    return {
        educations: state.get('educations').get('list')
    }
}

function mapDispatchToProps(dispatch) {
    return {}
}
