import React, {Component} from 'react';
import {Col} from 'reactstrap';
import {SocialButtons} from '../../components/index';
import './styles.scss';

export default class Footer extends Component {

    static displayName = 'Footer';

    render() {
        return (
            <footer className="footer">
                <Col md={{size: 10, offset: 1}}>
                <SocialButtons className='social-buttons_lg padding-xlg social-buttons-theme-dark'/>
                <div className='footer__content'>
                    <span className='padding'>IVANOV &copy; 2018. All rights reserved.</span>
                </div>
                </Col>
            </footer>
        );
    }
}
