import React, {Component} from 'react';
import routes from '../../routes';
import {Menu} from '../index';
import './styles.scss';

export default class Header extends Component {

    static displayName = 'Header';

    render() {
        return (
            <header className="header">
                <Menu fixed={true} routes={routes}/>
            </header>
        );
    }
}
