import React, {Component} from 'react';
import {Container} from 'reactstrap';
import {
    PageContainer,
    HomeInfo,
    Caption
} from '../../components/index';
import {RecentArticlesList, WorksList, EducationsList, SkillsList, ContactForm} from '../index';
import './styles.scss';

export default class Home extends Component {

    static displayName = 'Home';

    componentDidMount() {
        document.title = "Home";
    }

    render() {
        return (
            <PageContainer id="home" fluid={true} className="home-page">
                <HomeInfo/>
                <section className="home-page__back">
                    <EducationsList/>
                </section>
                <div className="home-page__back-reverse">
                    <SkillsList/>
                </div>
                <section className="home-page__back">
                    <Container>
                        <Caption className='kv-caption_center pd-top_lg pd-bm kv-caption-theme-light'>
                            <h1 className="title">SOME OF MY WORKS</h1>
                        </Caption>
                        <WorksList isBtnLoad={true}/>
                    </Container>
                </section>
                <section className="home-page__back-reverse">
                    <Container>
                        <Caption className='kv-caption_center padding-lg kv-caption-theme-light'>
                            <h1 className="title">BLOG POSTS</h1>
                        </Caption>
                        <RecentArticlesList isBtnLoad={true}/>
                    </Container>
                </section>
                <section className="home-page__back">
                    <Container>
                        <Caption className='kv-caption_center padding-lg kv-caption-theme-light'>
                            <h1>contact me</h1>
                        </Caption>
                        <ContactForm/>
                    </Container>
                </section>
            </PageContainer>
        );
    }
}
