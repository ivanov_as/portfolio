import About from './about';
import Blog from './blog';
import BlogSingle from './blog-single';
import CategoriesList from './categories-list';
import CategoriesMenuList from './categories-menu-list';
import CommentsList from './comments-list';
import Contact from './contact';
import EducationsList from './educations-list';
import Footer from './footer';
import Header from './header';
import Home from './home';
import ButtonUp from './button-up';
import Menu from './menu';
import RecentArticlesList from './articles-list';
import RelatedArticlesList from './related-articles-list';
import Root from './root';
import SkillsList from './skills-list';
import TagsList from './tags-list';
import Work from './work';
import WorkSingle from './work-single';
import WorksList from './works-list';
import ContactForm from './contact-form';

export {
    RecentArticlesList,
    RelatedArticlesList,
    Root,
    SkillsList,
    TagsList,
    Work,
    WorkSingle,
    WorksList,
    ButtonUp,
    Menu,
    Home,
    Footer,
    Header,
    EducationsList,
    Contact,
    CategoriesMenuList,
    About,
    CommentsList,
    BlogSingle,
    Blog,
    CategoriesList,
    ContactForm
}