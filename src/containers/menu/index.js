import React, {Component} from 'react';
import PropTypes from 'prop-types';
import ImmutablePropTypes from 'react-immutable-proptypes';
import classNames from 'classnames';
import {Map} from 'immutable';
import {STYLE} from '../../constants/index';
import {Link} from 'react-router-dom';
import {SocialButtons} from '../../components/index';
import './styles.scss';

export default class Menu extends Component {

    static displayName = 'Menu';

    static defaultProps = {
        fixed: false
    };

    static propTypes = {
        fixed: PropTypes.bool,
        routes: ImmutablePropTypes.listOf(ImmutablePropTypes.mapContains({
            caption: PropTypes.string.isRequired,
            path: PropTypes.string.isRequired,
            component: PropTypes.func.isRequired
        }))
    };

    state = {
        data: Map({
            isOpen: false,
            isUp: true
        })
    };

    componentDidMount() {
        document.addEventListener('scroll', this.onScroll, false);
    }

    componentWillUnmount() {
        document.removeEventListener('scroll', this.onScroll, false);
    }

    onScroll = () => {
        this.setState(state => ({data: state.data.set('isUp', document.documentElement.scrollTop <= 0)}));
    };

    toggleMobileMenu = () => {
        this.setState(state => ({data: state.data.set('isOpen', !state.data.get('isOpen'))}));
    };

    renderMenuItems = () => {
        return this.props.routes.map((item, idx) => {
            return (
                <div className="menu-item" key={idx}>
                    <Link to={item.get('path')} onClick={() => {
                        this.toggleMobileMenu();
                    }}>{item.get('caption')}</Link>
                </div>
            );
        })
    };

    render() {
        const {
            data
        } = this.state;

        /** Инициализация стилевых классов*/
        let classMenuHeader = classNames('menu-header', {
            'menu-wrapper-recursive': !data.get('isUp') && !data.get('isOpen'),
            [STYLE.FIXED]: this.props.fixed
        });
        let classMenuItems = classNames('menu-items', {
            [STYLE.FIXED]: this.props.fixed,
            [STYLE.HIDDEN]: !data.get('isOpen')
        });
        let classBtnMobileMenu = classNames('button-toggle', {
            'cross': data.get('isOpen')
        });
        let classRightBack = classNames('right-back', {
            [STYLE.HIDDEN]: !data.get('isOpen')
        });
        let classLeftBack = classNames('left-back', {
            [STYLE.HIDDEN]: !data.get('isOpen')
        });

        return (
            <menu className="menu-wrapper" ref="menu">
                <div className={classMenuHeader}>
                    <div className="menu-logo">
                        <SocialButtons className='social-buttons_sm social-buttons-theme-light'/>
                    </div>
                    <div className="menu-content">
                        {this.props.children}
                    </div>
                    <div className="menu-mobile">
                        <div className={classBtnMobileMenu} onClick={this.toggleMobileMenu}>
                            <div className="space"/>
                            <div className="space"/>
                            <div className="space"/>
                        </div>
                    </div>
                </div>
                <div className={classMenuItems}>
                    {this.renderMenuItems()}
                </div>
                <div className="blocks">
                    <div className={classRightBack}/>
                    <div className={classLeftBack}/>
                </div>
            </menu>
        );
    }
}
