import React, {Component} from 'react';
import {connect} from 'react-redux';
import ImmutablePropTypes from 'react-immutable-proptypes';
import {ArticleRelatedItem} from '../../components/index';
import {article} from '../../constants';
import './styles.scss';

@connect(mapStateToProps, mapDispatchToProps)
export default class RelatedArticlesList extends Component {

    static displayName = 'RelatedArticlesList';

    static propTypes = {
        articles: ImmutablePropTypes.listOf(article)
    };

    componentWillReceiveProps(newProps) {
        this.setState(newProps.articles);
    }

    render() {
        const {
            articles
        } = this.props;

        return (
            <div className='related-articles'>
                {articles.map((item) => {
                    return (<ArticleRelatedItem key={item.get('id')} article={item}/>);
                })}
            </div>
        )
    }
}

function mapStateToProps(state) {
    return {
        articles: state.get('articles').get('list').get('rows')
    }
}

function mapDispatchToProps(dispatch) {
    return {}
}
