import React, {Component} from 'react';
import routes from '../../routes';
import {Route, Switch} from 'react-router-dom';
import {NotFound} from '../../components/index';
import {Home, About, Contact, Work, Blog, BlogSingle, WorkSingle, ButtonUp, Header, Footer} from '../index';

export default class Root extends Component {

    static displayName = 'Root';

    renderRoutes = () => {
        return routes.map((route, idx) => {
            return (<Route key={idx} exact={route.get('exact') || false} path={route.get('path')}
                           component={route.get('component')}/>);
        })
    };

    render() {
        return (
            <div>
                <Header/>
                <main>
                        <Switch>
                            <Route exact={true} path="/" component={Home}/>
                            <Route path="/work/:id" component={WorkSingle}/>
                            <Route path="/work" component={Work}/>
                            <Route path="/blog/:id" component={BlogSingle}/>
                            <Route path="/blog" component={Blog}/>
                            <Route path="/contact" component={Contact}/>
                            <Route path="/about" component={About}/>
                            <Route path="**" component={NotFound}/>
                        </Switch>
                </main>
                <Footer/>
                <ButtonUp/>
            </div>
        );
    }
}
