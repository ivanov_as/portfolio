import React, {Component} from 'react';
import {connect} from 'react-redux';
import PropTypes from 'prop-types';
import ImmutablePropTypes from 'react-immutable-proptypes';
import {Container} from 'reactstrap';
import {SkillItem, Caption} from '../../components/index';
import {skill} from '../../constants';
import {listSkill} from '../../actions/skills-action';
import {bindActionCreators} from 'redux';
import './styles.scss';

@connect(mapStateToProps, mapDispatchToProps)
export default class SkillsList extends Component {

    static displayName = 'SkillsList';

    static defaultProps = {
        fluid: false
    };

    static propTypes = {
        fluid: PropTypes.bool,
        skills: ImmutablePropTypes.listOf(skill)
    };

    componentDidMount() {
        this.props.listSkill();
    }

    componentWillReceiveProps(newProps) {
        this.setState(newProps.skills);
    }

    render() {
        const {
            skills,
            fluid
        } = this.props;

        return (
            <Container fluid={fluid} className="skills pd-bm_lg">
                <Container>
                    <Caption className='kv-caption_center padding-lg kv-caption-theme-light'><h1>Skills</h1></Caption>
                    <div className="header-icon"><i className="fa fa-cube" aria-hidden="true"/></div>
                    {skills.map((item) => {
                        return (<SkillItem className='mg-bm_lg' key={item.get('id')} skill={item}/>);
                    })}
                </Container>
            </Container>
        )
    }
}

function mapStateToProps(state) {
    return {
        skills: state.get('skills').get('list')
    }
}

function mapDispatchToProps(dispatch) {
    return {
        listSkill: bindActionCreators(listSkill, dispatch)
    }
}
