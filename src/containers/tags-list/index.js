import React, {Component} from 'react';
import {connect} from 'react-redux';
import ImmutablePropTypes from 'react-immutable-proptypes';
import PropTypes from 'prop-types';
import {TagItem, BlockWithTitle} from '../../components/index';
import {tag} from '../../constants';
import {listTag} from '../../actions/tags-action';
import {bindActionCreators} from 'redux';
import './styles.scss';

@connect(mapStateToProps, mapDispatchToProps)
export default class TagsList extends Component {

    static displayName = 'TagsList';

    static propTypes = {
        tags: ImmutablePropTypes.listOf(tag),
        className: PropTypes.string
    };

    componentDidMount() {
        this.props.listTag();
    }

    componentWillReceiveProps(newProps) {
        this.setState(newProps.tags);
    }

    render() {
        const {
            tags,
            className
        } = this.props;

        return (
            <div className={`tags ${className || ''}`}>
                <BlockWithTitle title="Tags">
                    {tags.map((item) => {
                        return (<TagItem key={item.get('id')} tag={item}/>);
                    })}
                </BlockWithTitle>
            </div>
        )
    }
}

function mapStateToProps(state) {
    return {
        tags: state.get('tags').get('list')
    }
}

function mapDispatchToProps(dispatch) {
    return {
        listTag: bindActionCreators(listTag, dispatch)
    }
}
