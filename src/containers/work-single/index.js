import React, {Component} from 'react';
import {connect} from 'react-redux';
import {PageContainer, HeaderTitle, WorkDetail} from '../../components/index';
import './styles.scss';

@connect(mapStateToProps, mapDispatchToProps)
export default class WorkSingle extends Component {

    static displayName = 'WorkSingle';

    render() {

        const {
            work
        } = this.props;

        return (
            <PageContainer fluid={true} id="work" className="work-single">
                <HeaderTitle caption="PORTFOLIOS"
                             description="Lorem ipsum dolor sit amet, consectetur eiusmod tempor incididunt ut labore et dolore magna aliqua."/>
                <WorkDetail work={work}/>
            </PageContainer>
        );
    }
}

function mapStateToProps(state, ownProps) {
    return {
        work: state.get('works').get('list').get('rows').find((item => item.get('id') === Number(ownProps.match.params.id)))
    }
}

function mapDispatchToProps(dispatch) {
    return {}
}
