import React, {Component} from 'react';
import {connect} from 'react-redux';
import WorksList from '../works-list/index';
import {Map} from 'immutable';
import {Container, Row, Col} from 'reactstrap';
import {PageContainer, HeaderTitle} from '../../components/index';
import {CategoriesMenuList} from '../index';
import './styles.scss';

@connect(mapStateToProps, mapDispatchToProps)
export default class Work extends Component {

    static displayName = 'Work';

    state = {
        search: Map()
    };

    componentDidMount() {
        document.title = "Works";
    }

    componentWillReceiveProps(newProps) {
        this.setState((state) => ({search: state.search.set('category', newProps.category_active)}))
    }

    render() {
        const {
            search
        } = this.state;

        return (
            <PageContainer fluid={true} id="work" className="work-page">
                <HeaderTitle caption="PORTFOLIOS"
                             description="Lorem ipsum dolor sit amet, consectetur eiusmod tempor incididunt ut labore et dolore magna aliqua."/>
                <Container>
                    <Col>
                        <CategoriesMenuList className='pd-top_lg pd-sm'/>
                    </Col>
                    <Col md={{size: 8, offset: 2}}>
                        <Row>
                            <WorksList search={search.category} isBtnLoad={true}/>
                        </Row>
                    </Col>
                </Container>
            </PageContainer>
        );
    }
}

function mapStateToProps(state) {
    return {
        category_active: state.get('categories').get('active')
    }
}

function mapDispatchToProps(dispatch) {
    return {}
}
