import React, {Component} from 'react';
import {connect} from 'react-redux';
import PropTypes from 'prop-types';
import {Container} from 'reactstrap';
import {bindActionCreators} from 'redux';
import ImmutablePropTypes from 'react-immutable-proptypes';
import {WorkItem, Button, Pagination, ToolBar, DataNotFound} from '../../components/index';
import {listWork, selectWork} from '../../actions/works-action';
import {category, work} from '../../constants';
import {dataSet} from '../../hoc/dataset';
import './styles.scss';

@dataSet()
@connect(mapStateToProps, mapDispatchToProps)
export default class WorksList extends Component {

    static displayName = 'WorksList';

    static defaultProps = {
        isBtnLoad: false,
        isPagination: false
    };

    static propTypes = {
        isBtnLoad: PropTypes.bool,
        isPagination: PropTypes.bool,
        category_active: category,
        works: ImmutablePropTypes.listOf(work)
    };

    componentDidMount() {
        this.props.listWork();
    }

    get works() {
        return this.props.works;
    }

    clickBtnPagination = (offset) => {
        const {listWork, dsNextPage} = this.props;
        dsNextPage(offset, listWork);
    };

    clickBtnMore = () => {
        const {listWork, dsNextPage} = this.props;
        dsNextPage(undefined, listWork);
    };

    renderButtons = () => {
        const {
            isPagination,
            isBtnLoad
        } = this.props;
        return (
            <ToolBar className='kv-toolbar_center padding-lg'>
                {isBtnLoad ? (<Button className='kv-btn-lg kv-btn-theme-dark' onClick={this.clickBtnMore}>more
                    load</Button>) : null}
                {isPagination ? (
                    <Pagination limit={5} size={this.works.size} onClick={this.clickBtnPagination}/>) : null}
            </ToolBar>)
    };

    renderItem = () => {
        const {
            selectWork
        } = this.props;

        if (this.works.size) {
            return this.works.map((work) => (
                <WorkItem className='mg-top mg-left_xs mg-right_xs' key={work.get('id')} work={work} selectWork={() => {
                    selectWork(work)
                }}/>
            ))
        } else {
            return (
                <Container>
                    <DataNotFound className='data-not-found-theme-light'/>
                </Container>
            )
        }
    };

    render() {

        return (
            <div className="works">
                <div className="works__items">
                    {this.renderItem()}
                </div>
                {this.renderButtons()}
            </div>
        )
    }
}

function mapStateToProps(state) {
    return {
        works: state.get('works').get('list').get('rows'),
        count: state.get('works').get('list').get('count'),
        category_active: state.get('categories').get('active')
    }
}

function mapDispatchToProps(dispatch) {
    return {
        selectWork: bindActionCreators(selectWork, dispatch),
        listWork: bindActionCreators(listWork, dispatch)
    }
}
