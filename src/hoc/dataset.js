import {Map} from 'immutable';
import React from 'react';

export const dataSet = () => (Component) => {

    return class extends React.Component {

        static displayName = `dataSet(${Component.displayName || Component.name || 'Component'})`;

        state = {
            dataSet: Map({
                offset: 0
            })
        };

        get offset() {
            return this.state.dataSet.get('offset');
        };


        dsNextPage = (offset, cb) => {
            this.setState((state) => ({dataSet: state.dataSet.set('offset', (offset) ? offset : this.offset + 1)}));
            if (cb) {
                /** При указании offset работа идет с pagination */
                (offset !== undefined) ? cb(offset, true) : cb(this.offset)
            }
        };

        dsPriorPage = (cb) => {
            this.setState((state) => ({dataSet: state.dataSet.set('offset', this.offset - 1)}));
            if (cb) cb();
        };

        render() {
            return (<Component
                {...this.props}
                {...this.state}
                dsNextPage={this.dsNextPage}
                dsPriorPage={this.dsPriorPage}
            />)
        }
    }
};
