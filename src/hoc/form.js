import {Map} from 'immutable';
import React from 'react';

export const form = () => (Component) => {

    return class extends React.Component {

        static displayName = `form(${Component.displayName || Component.name || 'Component'})`;

        state = {
            dataForm: Map()
        };

        setData = (key, value) => {
            this.setState(state => ({dataForm: state.dataForm.set(key, value)}));
        };

        submitData = (callback) => {
            if (callback) callback(this.state.dataForm);
        };

        render() {
            return (<Component {...this.props} {...this.state} setData={this.setData} submitData={this.submitData}/>)
        }
    }
};
