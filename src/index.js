import React from 'react';
import ReactDOM from 'react-dom';
import 'bootstrap/dist/css/bootstrap.css';
import 'font-awesome/css/font-awesome.min.css';
import './index.scss';
import registerServiceWorker from './registerServiceWorker';
import {createStore, applyMiddleware} from 'redux';
import {combineReducers} from 'redux-immutable';
import {Provider} from 'react-redux';
import reducers from './reducers';
import {routerMiddleware} from 'react-router-redux';
import {Root} from './containers/index';
import createHashHistory from 'history/createHashHistory';
import {ConnectedRouter} from 'react-router-redux';
import axios from 'axios';
import axiosMiddleware from 'redux-axios-middleware';

const client = axios.create({
    baseURL:'http://127.0.0.1:3001/api/v2',
    headers: {
        'Access-Control-Allow-Origin' : '*',
        'Content-Type': 'application/json'
    }
});

const history = createHashHistory();
const root = document.getElementById('root');

const store = createStore(
    combineReducers({
        ...reducers
    }),
    applyMiddleware(routerMiddleware(history), axiosMiddleware(client))
);

store.subscribe(() => {
    //console.log(store.getState().get('works').get('list'));
});

ReactDOM.render(
    <Provider store={store}>
        <ConnectedRouter history={history}>
            <Root/>
        </ConnectedRouter>
    </Provider>,
    root);
registerServiceWorker();
