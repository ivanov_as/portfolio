import {Article} from '../../constants/action-types';
import Immutable from 'immutable';
import {handleActions} from 'redux-actions';

const initialState = Immutable.Map();

export default handleActions({
    [Article.SELECTED]: (state, action) => (action.payload)
}, initialState);
