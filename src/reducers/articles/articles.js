import {Article} from '../../constants/action-types';
import {handleActions} from 'redux-actions';
import Immutable from 'immutable';

const initialState = Immutable.Map({
    count: 0,
    rows: Immutable.List()
});

export default handleActions({
    [Article.FETCH_SUCCESS]: (state, action) => {
        let data = Immutable.fromJS(action.payload.data.data);
        let rows = (action.meta.previousAction.meta.isClear) ? data.get('rows') : state.get('rows').toSet().union(data.get('rows').toSet()).toList();
        return Immutable.Map({
            count: data.get('count'),
            rows
        })
},
    [Article.FETCH_FAILURE]: (state) => {
        console.debug(Article.FETCH_FAILURE)
    },
    [Article.CREATE_SUCCESS]: (state, action) => ((action.payload) ? state.push(action.payload) : state),
    [Article.CREATE_FAILURE]: (state) => (state),
    [Article.UPDATE_SUCCESS]: (state, action) => (state.map((record) => ((record.get('id') === action.payload.get('id')) ? action.payload : record))),
    [Article.UPDATE_FAILURE]: (state) => (state),
    [Article.REMOVE_SUCCESS]: (state, action) => (state.filter((record) => (record.get('id') !== action.payload.get('id')))),
    [Article.REMOVE_FAILURE]: (state) => (state)
}, initialState);