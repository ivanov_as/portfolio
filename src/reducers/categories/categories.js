import {Category} from '../../constants/action-types';
import {handleActions} from 'redux-actions';
import Immutable from 'immutable';

const initialState = Immutable.List();

export default handleActions({
    [Category.FETCH_SUCCESS]: (state, action) => (Immutable.fromJS(action.payload.data.data)),
    [Category.FETCH_FAILURE]: () => {
        console.debug(Category.FETCH_FAILURE)
    }
}, initialState);