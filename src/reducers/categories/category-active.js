import {Category} from "../../constants/action-types";
import {Map} from 'immutable';
import {handleActions} from 'redux-actions';

const initialState = Map({
    id: 1,
    category_name: "Все"
});

export default handleActions({
    [Category.SELECTED]: (state, action) => ((action.payload.get('id') === 0) ? null : action.payload)
}, initialState);
