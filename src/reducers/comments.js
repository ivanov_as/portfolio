import {ArticleComment} from '../constants/action-types';
import Immutable from 'immutable';
import {handleActions} from 'redux-actions';

const initialState = Immutable.fromJS([
    {
        id: 1,
        name: 'Shirley C. Williams',
        desc: 'Хорошая статья',
        mail: 'red.systems@yandex.ru',
        src: 'https://lh3.googleusercontent.com/-LgV3F2jpJZM/WKrDhqYA8zI/AAAAAAAAAA0/zSBzGpYmOgY2t_BboZwx42ULYSBZpeqRgCEwYBhgL/w139-h140-p/IMG_1387.jpg'
    },
    {
        id: 2,
        name: 'Zachary Shook',
        desc: 'Отличная статья',
        mail: 'red.systems@yandex.ru',
        src: 'https://lh3.googleusercontent.com/-LgV3F2jpJZM/WKrDhqYA8zI/AAAAAAAAAA0/zSBzGpYmOgY2t_BboZwx42ULYSBZpeqRgCEwYBhgL/w139-h140-p/IMG_1387.jpg'
    },
    {
        id: 3,
        name: 'Kenneth S. McKeown',
        desc: 'Нужно добавить больше данных',
        mail: 'red.systems@yandex.ru',
        src: 'https://lh3.googleusercontent.com/-LgV3F2jpJZM/WKrDhqYA8zI/AAAAAAAAAA0/zSBzGpYmOgY2t_BboZwx42ULYSBZpeqRgCEwYBhgL/w139-h140-p/IMG_1387.jpg'
    }
]);

export default handleActions({
    [ArticleComment.FETCH_SUCCESS]: (state, action) => {},
    [ArticleComment.FETCH_FAILURE]: (state) => (state)
}, initialState);
