import {Education} from '../../constants/action-types';
import Immutable from 'immutable';
import {handleActions} from 'redux-actions';

const initialState = Immutable.fromJS([
    {
        id: 1,
        institute: 'Санкт-Петербургский государственный политехнический университет',
        pulpit: 'Экономики и менеджмента',
        year: 2015
    },
    {
        id: 2,
        institute: 'Санкт-Петербургский государственный политехнический университет',
        pulpit: 'Технологии веб-разработки',
        year: 2018
    }
]);

export default handleActions({
    [Education.FETCH_SUCCESS]: (state, action) => (Immutable.List(action.payload)),
    [Education.FETCH_FAILURE]: (state) => (state)
}, initialState);
