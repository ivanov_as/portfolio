import {routerReducer} from 'react-router-redux';
import {combineReducers} from 'redux-immutable';
import worksReducer from './works/works';
import workActiveReducer from './works/work-active';
import categoriesReducer from './categories/categories';
import categoryActiveReducer from './categories/category-active';
import articlesReducer from './articles/articles';
import articleActiveReducer from './articles/article-active';
import educationsReducer from './educations/educations';
import skillsReducer from './skills/skills';
import articleCommentsReducer from './comments';
import tagsReducer from './tags/tags';
import messagesReducer from './messages';

const reducers = {
    works: combineReducers({
        list: worksReducer,
        active: workActiveReducer
    }),
    categories: combineReducers({
        list: categoriesReducer,
        active: categoryActiveReducer
    }),
    articles: combineReducers({
        list: articlesReducer,
        active: articleActiveReducer
    }),
    educations: combineReducers({
        list: educationsReducer
    }),
    skills: combineReducers({
        list: skillsReducer
    }),
    tags: combineReducers({
        list: tagsReducer
    }),
    comments: combineReducers({
        list: articleCommentsReducer
    }),
    messages: combineReducers({
        list: messagesReducer
    }),
    routing: routerReducer
};

export default reducers;