import {Message} from '../constants/action-types';
import Immutable from 'immutable';
import {handleActions} from "redux-actions";

const initialState = Immutable.List();

export default handleActions({
    [Message.CREATE_SUCCESS]: () => {
        console.log(Message.CREATE_SUCCESS)
    },
    [Message.CREATE_FAILURE]: () => {
        console.debug(Message.FETCH_FAILURE)
    }
}, initialState);
