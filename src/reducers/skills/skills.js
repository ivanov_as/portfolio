import {Skill} from '../../constants/action-types';
import Immutable from 'immutable';
import {handleActions} from 'redux-actions';

const initialState = Immutable.List();

export default handleActions({
    [Skill.FETCH_SUCCESS]: (state, action) => (Immutable.fromJS(action.payload.data.data)),
    [Skill.FETCH_FAILURE]: () => {
        console.debug(Skill.FETCH_FAILURE)
    }
}, initialState);
