import {Tag} from '../../constants/action-types';
import Immutable from 'immutable';
import {handleActions} from 'redux-actions';

const initialState = Immutable.List();

export default handleActions({
    [Tag.FETCH_SUCCESS]: (state, action) => (Immutable.fromJS(action.payload.data.data)),
    [Tag.FETCH_FAILURE]: () => {
        console.debug(Tag.FETCH_FAILURE)
    }
}, initialState);
