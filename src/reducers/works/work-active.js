import {Work} from '../../constants/action-types';
import {Map} from 'immutable';
import {handleActions} from 'redux-actions';

const initialState = Map();

export default handleActions({
    [Work.SELECTED]: (state, action) => (action.payload)
}, initialState);
