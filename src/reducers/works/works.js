import {Work} from '../../constants/action-types';
import Immutable from 'immutable';
import {handleActions} from "redux-actions";

const initialState = Immutable.Map({
    count: 0,
    rows: Immutable.List()
});

export default handleActions({
    [Work.FETCH_SUCCESS]: (state, action) => {
        let data = Immutable.fromJS(action.payload.data.data);
        let rows = (action.meta.previousAction.meta.isClear) ? data.get('rows') : state.get('rows').toSet().union(data.get('rows').toSet()).toList();
        return Immutable.Map({
            count: data.get('count'),
            rows
        })
    },
    [Work.FETCH_FAILURE]: (state) => {
        console.debug(Work.FETCH_FAILURE)
    },
    [Work.CREATE_SUCCESS]: (state, action) => ((action.payload) ? state.push(action.payload) : state),
    [Work.CREATE_FAILURE]: (state) => (state),
    [Work.UPDATE_SUCCESS]: (state, action) => (state.map((record) => ((record.get('id') === action.payload.get('id')) ? action.payload : record))),
    [Work.UPDATE_FAILURE]: (state) => (state),
    [Work.REMOVE_SUCCESS]: (state, action) => (state.filter((record) => (record.get('id') !== action.payload.get('id')))),
    [Work.REMOVE_FAILURE]: (state) => (state)
}, initialState);
