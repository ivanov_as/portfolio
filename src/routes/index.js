import Immutable from 'immutable';
import Home from '../containers/home/index';
import About from '../containers/about/index';
import Contact from '../containers/contact/index';
import Work from '../containers/work/index';
import Blog from '../containers/blog/index';

const ROUTES = Immutable.fromJS([
    {
        path: '/',
        exact: true,
        caption: 'HOME',
        component: Home
    },
    {
        path: '/about',
        caption: 'ABOUT',
        component: About
    },
    {
        path: '/contact',
        caption: 'CONTACT',
        component: Contact
    },
    {
        path: '/blog',
        caption: 'BLOG',
        component: Blog
    },
    {
        path: '/work',
        caption: 'WORK',
        component: Work
    }
]);

export default ROUTES;