import chai from 'chai';
import chaiImmutable from 'chai-immutable';
import {Map} from 'immutable';
import {Article} from '../../constants/action-types';
import * as articlesAction from '../../actions/articles-action';
import {REMOTE_REQUEST} from "../../constants";

const expect = chai.expect;

const ARTICLE_RECORD = Map({
    id: 1,
    name: 'test article',
    desc: 'Test desc'
});

chai.use(chaiImmutable);

describe('Test article actions', () => {
    it(`action ${Article.FETCH}`, () => {
        expect(articlesAction.listArticle()).to.deep.equal({
            type: Article.FETCH, 
            meta: REMOTE_REQUEST
        });
    });
    it(`action ${Article.FETCH_SUCCESS}`, () => {
        expect(articlesAction.listArticleSuccess(ARTICLE_RECORD)).to.deep.equal({
            type: Article.FETCH_SUCCESS, 
            payload: ARTICLE_RECORD
        });
    });
    it(`action ${Article.FETCH_FAILURE}`, () => {
        expect(articlesAction.listArticleFailure()).to.deep.equal({
            type: Article.FETCH_FAILURE
        });
    });
    it(`action ${Article.SELECTED}`, () => {
        expect(articlesAction.selectArticle(ARTICLE_RECORD)).to.deep.equal({
            type: Article.SELECTED, 
            payload: ARTICLE_RECORD
        });
    });
    it(`action ${Article.CREATE}`, () => {
        expect(articlesAction.createArticle(ARTICLE_RECORD)).to.deep.equal({
            type: Article.CREATE, 
            meta: REMOTE_REQUEST,
            payload: ARTICLE_RECORD
        });
    });
    it(`action ${Article.CREATE_SUCCESS}`, () => {
        expect(articlesAction.createArticleSuccess(ARTICLE_RECORD)).to.deep.equal({
            type: Article.CREATE_SUCCESS,
            payload: ARTICLE_RECORD
        });
    });
    it(`action ${Article.CREATE_FAILURE}`, () => {
        expect(articlesAction.createArticleFailure()).to.deep.equal({
            type: Article.CREATE_FAILURE
        });
    });
    it(`action ${Article.UPDATE}`, () => {
        expect(articlesAction.updateArticle(ARTICLE_RECORD)).to.deep.equal({
            type: Article.UPDATE, 
            meta: REMOTE_REQUEST,
            payload: ARTICLE_RECORD
        });
    });
    it(`action ${Article.UPDATE_SUCCESS}`, () => {
        expect(articlesAction.updateArticleSuccess(ARTICLE_RECORD)).to.deep.equal({
            type: Article.UPDATE_SUCCESS, 
            payload: ARTICLE_RECORD
        });
    });
    it(`action ${Article.UPDATE_FAILURE}`, () => {
        expect(articlesAction.updateArticleFailure()).to.deep.equal({
            type: Article.UPDATE_FAILURE
        });
    });
    it(`action ${Article.REMOVE}`, () => {
        expect(articlesAction.removeArticle(ARTICLE_RECORD)).to.deep.equal({
            type: Article.REMOVE, 
            meta: REMOTE_REQUEST,
            payload: ARTICLE_RECORD
        });
    });
    it(`action ${Article.REMOVE_SUCCESS}`, () => {
        expect(articlesAction.removeArticleSuccess(ARTICLE_RECORD)).to.deep.equal({
            type: Article.REMOVE_SUCCESS, 
            payload: ARTICLE_RECORD
        });
    });
    it(`action ${Article.REMOVE_FAILURE}`, () => {
        expect(articlesAction.removeArticleFailure()).to.deep.equal({
            type: Article.REMOVE_FAILURE
        });
    });
});