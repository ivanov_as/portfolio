import chai from 'chai';
import chaiImmutable from 'chai-immutable';
import {Map} from 'immutable';
import {Category} from '../../constants/action-types';
import * as categoriesAction from '../../actions/categories-action';
import {REMOTE_REQUEST} from "../../constants";

const expect = chai.expect;

const CATEGORY_RECORD = Map({
    id: 1,
    name: 'test category',
    count: 5
});

chai.use(chaiImmutable);

describe('Test category actions', () => {
    it(`action ${Category.FETCH}`, () => {
        expect(categoriesAction.listCategory()).to.deep.equal({
            type: Category.FETCH,
            meta: REMOTE_REQUEST
        });
    });
    it(`action ${Category.FETCH_SUCCESS}`, () => {
        expect(categoriesAction.listCategorySuccess(CATEGORY_RECORD)).to.deep.equal({
            type: Category.FETCH_SUCCESS,
            payload: CATEGORY_RECORD
        });
    });
    it(`action ${Category.FETCH_FAILURE}`, () => {
        expect(categoriesAction.listCategoryFailure()).to.deep.equal({
            type: Category.FETCH_FAILURE
        });
    });
    it(`action ${Category.SELECTED}`, () => {
        expect(categoriesAction.selectCategory(CATEGORY_RECORD)).to.deep.equal({
            type: Category.SELECTED,
            payload: CATEGORY_RECORD
        });
    });
    it(`action ${Category.CREATE}`, () => {
        expect(categoriesAction.createCategory(CATEGORY_RECORD)).to.deep.equal({
            type: Category.CREATE,
            meta: {remote: true},
            payload: CATEGORY_RECORD
        });
    });
    it(`action ${Category.CREATE_SUCCESS}`, () => {
        expect(categoriesAction.createCategorySuccess(CATEGORY_RECORD)).to.deep.equal({
            type: Category.CREATE_SUCCESS,
            payload: CATEGORY_RECORD
        });
    });
    it(`action ${Category.CREATE_FAILURE}`, () => {
        expect(categoriesAction.createCategoryFailure()).to.deep.equal({
            type: Category.CREATE_FAILURE
        });
    });
    it(`action ${Category.UPDATE}`, () => {
        expect(categoriesAction.updateCategory(CATEGORY_RECORD)).to.deep.equal({
            type: Category.UPDATE,
            meta: {remote: true},
            payload: CATEGORY_RECORD
        });
    });
    it(`action ${Category.UPDATE_SUCCESS}`, () => {
        expect(categoriesAction.updateCategorySuccess(CATEGORY_RECORD)).to.deep.equal({
            type: Category.UPDATE_SUCCESS,
            payload: CATEGORY_RECORD
        });
    });
    it(`action ${Category.UPDATE_FAILURE}`, () => {
        expect(categoriesAction.updateCategoryFailure()).to.deep.equal({
            type: Category.UPDATE_FAILURE
        });
    });
    it(`action ${Category.REMOVE}`, () => {
        expect(categoriesAction.removeCategory(CATEGORY_RECORD)).to.deep.equal({
            type: Category.REMOVE,
            meta: {remote: true},
            payload: CATEGORY_RECORD
        });
    });
    it(`action ${Category.REMOVE_SUCCESS}`, () => {
        expect(categoriesAction.removeCategorySuccess(CATEGORY_RECORD)).to.deep.equal({
            type: Category.REMOVE_SUCCESS,
            payload: CATEGORY_RECORD
        });
    });
    it(`action ${Category.REMOVE_FAILURE}`, () => {
        expect(categoriesAction.removeCategoryFailure()).to.deep.equal({
            type: Category.REMOVE_FAILURE
        });
    });
});