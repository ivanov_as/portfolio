import chai from 'chai';
import chaiImmutable from 'chai-immutable';
import {Map} from 'immutable';
import {Education} from '../../constants/action-types';
import * as educationsAction from '../../actions/educations-action';
import {REMOTE_REQUEST} from "../../constants";

const expect = chai.expect;

const EDUCATION_RECORD = Map({
    id: 2,
    institute: 'Санкт-Петербургский государственный политехнический университет',
    pulpit: 'Технологии веб-разработки',
    year: 2018
});

chai.use(chaiImmutable);

describe('Test education actions', () => {
        it(`action ${Education.FETCH}`, () => {
            expect(educationsAction.listEducation()).to.deep.equal({
                type: Education.FETCH,
                meta: REMOTE_REQUEST
            });
        });
        it(`action ${Education.FETCH_SUCCESS}`, () => {
            expect(educationsAction.listEducationSuccess(EDUCATION_RECORD)).to.deep.equal({
                type: Education.FETCH_SUCCESS,
                payload: EDUCATION_RECORD
            });
        });
        it(`action ${Education.FETCH_FAILURE}`, () => {
            expect(educationsAction.listEducationFailure()).to.deep.equal({
                type: Education.FETCH_FAILURE
            });
        });
        it(`action ${Education.CREATE}`, () => {
            expect(educationsAction.createEducation(EDUCATION_RECORD)).to.deep.equal({
                type: Education.CREATE,
                meta: REMOTE_REQUEST,
                payload: EDUCATION_RECORD
            });
        });
        it(`action ${Education.CREATE_SUCCESS}`, () => {
            expect(educationsAction.createEducationSuccess(EDUCATION_RECORD)).to.deep.equal({
                type: Education.CREATE_SUCCESS,
                payload: EDUCATION_RECORD
            });
        });
        it(`action ${Education.CREATE_FAILURE}`, () => {
            expect(educationsAction.createEducationFailure()).to.deep.equal({
                type: Education.CREATE_FAILURE
            });
        });
        it(`action ${Education.UPDATE}`, () => {
            expect(educationsAction.updateEducation(EDUCATION_RECORD)).to.deep.equal({
                type: Education.UPDATE,
                meta: REMOTE_REQUEST,
                payload: EDUCATION_RECORD
            });
        });
        it(`action ${Education.UPDATE_SUCCESS}`, () => {
            expect(educationsAction.updateEducationSuccess(EDUCATION_RECORD)).to.deep.equal({
                type: Education.UPDATE_SUCCESS,
                payload: EDUCATION_RECORD
            });
        });
        it(`action ${Education.UPDATE_FAILURE}`, () => {
            expect(educationsAction.updateEducationFailure()).to.deep.equal({
                type: Education.UPDATE_FAILURE
            });
        });
        it(`action ${Education.REMOVE}`, () => {
            expect(educationsAction.removeEducation(EDUCATION_RECORD)).to.deep.equal({
                type: Education.REMOVE,
                meta: REMOTE_REQUEST,
                payload: EDUCATION_RECORD
            });
        });
        it(`action ${Education.REMOVE_SUCCESS}`, () => {
            expect(educationsAction.removeEducationSuccess(EDUCATION_RECORD)).to.deep.equal({
                type: Education.REMOVE_SUCCESS,
                payload: EDUCATION_RECORD
            });
        });
        it(`action ${Education.REMOVE_FAILURE}`, () => {
            expect(educationsAction.removeEducationFailure()).to.deep.equal({
                type: Education.REMOVE_FAILURE
            });
        });
    }
);