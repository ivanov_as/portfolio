import chai from 'chai';
import chaiImmutable from 'chai-immutable';
import {Map} from 'immutable';
import {Skill} from '../../constants/action-types';
import * as skillsAction from '../../actions/skills-action';
import {REMOTE_REQUEST} from "../../constants";

const expect = chai.expect;

const SKILL_RECORD = Map({id: 1, name: 'Опыт разработки на Javascript (ES6)', percent: 1});

chai.use(chaiImmutable);

describe('Test skill actions', () => {

    it(`action ${Skill.FETCH}`, () => {
        expect(skillsAction.listSkill()).to.deep.equal({
            type: Skill.FETCH,
            meta: REMOTE_REQUEST
        });
    });
    it(`action ${Skill.FETCH_SUCCESS}`, () => {
        expect(skillsAction.listSkillSuccess(SKILL_RECORD)).to.deep.equal({
            type: Skill.FETCH_SUCCESS,
            payload: SKILL_RECORD
        });
    });
    it(`action ${Skill.FETCH_FAILURE}`, () => {
        expect(skillsAction.listSkillFailure()).to.deep.equal({
            type: Skill.FETCH_FAILURE
        });
    });
    it(`action ${Skill.CREATE}`, () => {
        expect(skillsAction.createSkill(SKILL_RECORD)).to.deep.equal({
            type: Skill.CREATE,
            meta: REMOTE_REQUEST,
            payload: SKILL_RECORD
        });
    });
    it(`action ${Skill.CREATE_SUCCESS}`, () => {
        expect(skillsAction.createSkillSuccess(SKILL_RECORD)).to.deep.equal({
            type: Skill.CREATE_SUCCESS,
            payload: SKILL_RECORD
        });
    });
    it(`action ${Skill.CREATE_FAILURE}`, () => {
        expect(skillsAction.createSkillFailure()).to.deep.equal({
            type: Skill.CREATE_FAILURE
        });
    });
    it(`action ${Skill.UPDATE}`, () => {
        expect(skillsAction.updateSkill(SKILL_RECORD)).to.deep.equal({
            type: Skill.UPDATE,
            meta: REMOTE_REQUEST,
            payload: SKILL_RECORD
        });
    });
    it(`action ${Skill.UPDATE_SUCCESS}`, () => {
        expect(skillsAction.updateSkillSuccess(SKILL_RECORD)).to.deep.equal({
            type: Skill.UPDATE_SUCCESS,
            payload: SKILL_RECORD
        });
    });
    it(`action ${Skill.UPDATE_FAILURE}`, () => {
        expect(skillsAction.updateSkillFailure()).to.deep.equal({
            type: Skill.UPDATE_FAILURE
        });
    });
    it(`action ${Skill.REMOVE}`, () => {
        expect(skillsAction.removeSkill(SKILL_RECORD)).to.deep.equal({
            type: Skill.REMOVE,
            meta: REMOTE_REQUEST,
            payload: SKILL_RECORD
        });
    });
    it(`action ${Skill.REMOVE_SUCCESS}`, () => {
        expect(skillsAction.removeSkillSuccess(SKILL_RECORD)).to.deep.equal({
            type: Skill.REMOVE_SUCCESS,
            payload: SKILL_RECORD
        });
    });
    it(`action ${Skill.REMOVE_FAILURE}`, () => {
        expect(skillsAction.removeSkillFailure()).to.deep.equal({
            type: Skill.REMOVE_FAILURE
        });
    });
});