import chai from 'chai';
import chaiImmutable from 'chai-immutable';
import {Map} from 'immutable';
import {Work} from '../../constants/action-types';
import * as worksAction from '../../actions/works-action';
import {REMOTE_REQUEST} from "../../constants";

const expect = chai.expect;

const WORK_RECORD = Map({
    id: 1,
    name: 'test work'
});

chai.use(chaiImmutable);

describe('Test work actions', () => {
    
    it(`action ${Work.FETCH}`, () => {
        expect(worksAction.listWork()).to.deep.equal({
            type: Work.FETCH,
            meta: REMOTE_REQUEST
        });
    });
    it(`action ${Work.FETCH_SUCCESS}`, () => {
        expect(worksAction.listWorkSuccess(WORK_RECORD)).to.deep.equal({
            type: Work.FETCH_SUCCESS, payload: WORK_RECORD
        });
    });
    it(`action ${Work.FETCH_FAILURE}`, () => {
        expect(worksAction.listWorkFailure()).to.deep.equal({
            type: Work.FETCH_FAILURE
        });
    });
    it(`action ${Work.SELECTED}`, () => {
        expect(worksAction.selectWork(WORK_RECORD)).to.deep.equal({
            type: Work.SELECTED,
            payload: WORK_RECORD
        });
    });
    it(`action ${Work.CREATE}`, () => {
        expect(worksAction.createWork(WORK_RECORD)).to.deep.equal({
            type: Work.CREATE,
            meta: REMOTE_REQUEST,
            payload: WORK_RECORD
        });
    });
    it(`action ${Work.CREATE_SUCCESS}`, () => {
        expect(worksAction.createWorkSuccess(WORK_RECORD)).to.deep.equal({
            type: Work.CREATE_SUCCESS,
            payload: WORK_RECORD
        });
    });
    it(`action ${Work.CREATE_FAILURE}`, () => {
        expect(worksAction.createWorkFailure()).to.deep.equal({
            type: Work.CREATE_FAILURE
        });
    });
    it(`action ${Work.UPDATE}`, () => {
        expect(worksAction.updateWork(WORK_RECORD)).to.deep.equal({
            type: Work.UPDATE,
            meta: REMOTE_REQUEST,
            payload: WORK_RECORD
        });
    });
    it(`action ${Work.UPDATE_SUCCESS}`, () => {
        expect(worksAction.updateWorkSuccess(WORK_RECORD)).to.deep.equal({
            type: Work.UPDATE_SUCCESS,
            payload: WORK_RECORD
        });
    });
    it(`action ${Work.UPDATE_FAILURE}`, () => {
        expect(worksAction.updateWorkFailure()).to.deep.equal({
            type: Work.UPDATE_FAILURE
        });
    });
    it(`action ${Work.REMOVE}`, () => {
        expect(worksAction.removeWork(WORK_RECORD)).to.deep.equal({
            type: Work.REMOVE,
            meta: REMOTE_REQUEST,
            payload: WORK_RECORD
        });
    });
    it(`action ${Work.REMOVE_SUCCESS}`, () => {
        expect(worksAction.removeWorkSuccess(WORK_RECORD)).to.deep.equal({
            type: Work.REMOVE_SUCCESS,
            payload: WORK_RECORD
        });
    });
    it(`action ${Work.REMOVE_FAILURE}`, () => {
        expect(worksAction.removeWorkFailure()).to.deep.equal({
            type: Work.REMOVE_FAILURE
        });
    });
});