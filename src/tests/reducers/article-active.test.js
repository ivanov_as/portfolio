import chai from 'chai';
import chaiImmutable from 'chai-immutable';
import Immutable from 'immutable';
import articleActiveReducer from '../../reducers/articles/article-active';
import {Article} from '../../constants/action-types';
import * as articlesAction from '../../actions/articles-action';

const expect = chai.expect;

const ARTICLE_RECORD = Immutable.Map({
    id: 4,
    article_name: 'РАЗРАБОТКА АДАПТИВНЫХ ВЕБ-ИНТРЕФЕЙСОВ ЧАСТЬ 4',
    article_desc: 'Для разработки данного фреймворка требуется уделить особое внимание методологии БЭМ',
    article_url: 'https://www.youtube.com/embed/F0f3iov79q4'
});

chai.use(chaiImmutable);

describe('reducer article active', () => {
    it(`reducer on action ${Article.SELECTED}`, () => {
        expect(articleActiveReducer(null, articlesAction.selectArticle(ARTICLE_RECORD))).to.deep.equal(ARTICLE_RECORD);
    });
});
