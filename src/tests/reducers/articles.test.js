import chai from 'chai';
import chaiImmutable from 'chai-immutable';
import Immutable from 'immutable';
import articlesReducer from '../../reducers/articles/articles';
import * as articlesAction from '../../actions/articles-action';
import {Article} from '../../constants/action-types';

let stateInitial;

const expect = chai.expect;

const ARTICLE_RECORD = Immutable.Map({
    id: 4,
    article_name: 'РАЗРАБОТКА АДАПТИВНЫХ ВЕБ-ИНТРЕФЕЙСОВ ЧАСТЬ 4',
    article_desc: 'Для разработки данного фреймворка требуется уделить особое внимание методологии БЭМ',
    article_url: 'https://www.youtube.com/embed/F0f3iov79q4'
});

const ARTICLE_RECORD_MODIFY = Immutable.Map({
    id: 1,
    article_name: 'РАЗРАБОТКА АДАПТИВНЫХ ВЕБ-ИНТРЕФЕЙСОВ ЧАСТЬ 11',
    article_desc: 'Для разработки данного фреймворка требуется уделить особое внимание методологии БЭМ',
    article_url: 'https://www.youtube.com/embed/ftrn50AJa2w'
});

const ARTICLE_RECORD_REMOVE = Immutable.Map({
    id: 2,
    article_name: 'РАЗРАБОТКА АДАПТИВНЫХ ВЕБ-ИНТРЕФЕЙСОВ ЧАСТЬ 22',
    article_desc: 'Для разработки данного фреймворка требуется уделить особое внимание методологии БЭМ',
    article_url: 'https://www.youtube.com/embed/sbCgQJQNZKs'
});

chai.use(chaiImmutable);

describe('reducer articles', () => {
    beforeEach(() => {
        stateInitial = Immutable.fromJS([
            {
                id: 1,
                article_name: 'РАЗРАБОТКА АДАПТИВНЫХ ВЕБ-ИНТРЕФЕЙСОВ ЧАСТЬ 1',
                article_desc: 'Для разработки данного фреймворка требуется уделить особое внимание методологии БЭМ',
                article_url: 'https://www.youtube.com/embed/ftrn50AJa2w'
            },
            {
                id: 2,
                article_name: 'РАЗРАБОТКА АДАПТИВНЫХ ВЕБ-ИНТРЕФЕЙСОВ ЧАСТЬ 2',
                article_desc: 'Для разработки данного фреймворка требуется уделить особое внимание методологии БЭМ',
                article_url: 'https://www.youtube.com/embed/sbCgQJQNZKs'
            },
            {
                id: 3,
                article_name: 'РАЗРАБОТКА АДАПТИВНЫХ ВЕБ-ИНТРЕФЕЙСОВ ЧАСТЬ 3',
                article_desc: 'Для разработки данного фреймворка требуется уделить особое внимание методологии БЭМ',
                article_url: 'https://www.youtube.com/embed/F0f3iov79q4'
            }
        ]);
    });
    it(`reducer on action ${Article.FETCH_SUCCESS}`, () => {
        expect(articlesReducer(stateInitial, articlesAction.listArticleSuccess(stateInitial))).to.deep.equal(stateInitial);
    });
    it(`reducer on action ${Article.FETCH_FAILURE}`, () => {
        expect(articlesReducer(stateInitial, articlesAction.listArticleFailure)).to.deep.equal(stateInitial);
    });
    it(`reducer on action ${Article.CREATE_SUCCESS}`, () => {
        expect(articlesReducer(stateInitial, articlesAction.createArticleSuccess(ARTICLE_RECORD))).to.deep.equal(stateInitial.push(ARTICLE_RECORD));
    });
    it(`reducer on action ${Article.UPDATE_SUCCESS}`, () => {
        expect(articlesReducer(stateInitial, articlesAction.updateArticleSuccess(ARTICLE_RECORD_MODIFY))).to.deep.equal(stateInitial.set(0, ARTICLE_RECORD_MODIFY));
    });
    it(`reducer on action ${Article.UPDATE_FAILURE}`, () => {
        expect(articlesReducer(stateInitial, articlesAction.updateArticleFailure)).to.deep.equal(stateInitial);
    });
    it(`reducer on action ${Article.REMOVE_SUCCESS}`, () => {
        expect(articlesReducer(stateInitial, articlesAction.removeArticleSuccess(ARTICLE_RECORD_REMOVE))).to.deep.equal(stateInitial.delete(1));
    });
    it(`reducer on action ${Article.REMOVE_FAILURE}`, () => {
        expect(articlesReducer(stateInitial, articlesAction.removeArticleFailure)).to.deep.equal(stateInitial);
    });
});
