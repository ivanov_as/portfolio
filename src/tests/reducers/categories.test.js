import chai from 'chai';
import chaiImmutable from 'chai-immutable';
import Immutable from 'immutable';
import categoriesReducer from '../../reducers/categories/categories';
import * as categoriesAction from '../../actions/categories-action';
import {Category} from '../../constants/action-types';

let stateInitial;

const CATEGORY_RECORD = Immutable.Map({id: 4, category_name: 'Category 4'});

const CATEGORY_RECORD_MODIFY = Immutable.Map({id: 1, name: 'Категория 11'});

const CATEGORY_RECORD_REMOVE = Immutable.Map({id: 2, category_name: 'Категория 31'});

const expect = chai.expect;

chai.use(chaiImmutable);

describe('reducer categories', () => {
    beforeEach(() => {
        stateInitial = Immutable.fromJS([
            {id: 1, category_name: 'Категория 1'},
            {id: 2, category_name: 'Категория 2'},
            {id: 3, category_name: 'Категория 3'}
        ]);
    });
    it(`reducer on action ${Category.FETCH_SUCCESS}`, () => {
        expect(categoriesReducer(stateInitial, categoriesAction.listCategorySuccess(stateInitial))).to.deep.equal(stateInitial);
    });
    it(`reducer on action ${Category.FETCH_FAILURE}`, () => {
        expect(categoriesReducer(stateInitial, categoriesAction.listCategoryFailure)).to.deep.equal(stateInitial);
    });
    it(`reducer on action ${Category.CREATE_SUCCESS}`, () => {
        expect(categoriesReducer(stateInitial, categoriesAction.createCategorySuccess(CATEGORY_RECORD))).to.deep.equal(stateInitial.push(CATEGORY_RECORD));
    });
    it(`reducer on action ${Category.UPDATE_SUCCESS}`, () => {
        expect(categoriesReducer(stateInitial, categoriesAction.updateCategorySuccess(CATEGORY_RECORD_MODIFY))).to.deep.equal(stateInitial.set(0, CATEGORY_RECORD_MODIFY));
    });
    it(`reducer on action ${Category.UPDATE_FAILURE}`, () => {
        expect(categoriesReducer(stateInitial, categoriesAction.updateCategoryFailure)).to.deep.equal(stateInitial);
    });
    it(`reducer on action ${Category.REMOVE_SUCCESS}`, () => {
        expect(categoriesReducer(stateInitial, categoriesAction.removeCategorySuccess(CATEGORY_RECORD_REMOVE))).to.deep.equal(stateInitial.delete(1));
    });
    it(`reducer on action ${Category.REMOVE_FAILURE}`, () => {
        expect(categoriesReducer(stateInitial, categoriesAction.removeCategoryFailure)).to.deep.equal(stateInitial);
    });
});
