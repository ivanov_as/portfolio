import chai from 'chai';
import chaiImmutable from 'chai-immutable';
import Immutable from 'immutable';
import categoryActiveReducer from '../../reducers/categories/category-active';
import * as categoriesAction from '../../actions/categories-action';
import {Category} from '../../constants/action-types';

const expect = chai.expect;

const CATEGORY_RECORD = Immutable.Map({id: 4, category_name: 'Category 4'});

chai.use(chaiImmutable);

describe('reducer article active', () => {
    it(`reducer on action ${Category.SELECTED}`, () => {
        expect(categoryActiveReducer(null, categoriesAction.selectCategory(CATEGORY_RECORD))).to.deep.equal(CATEGORY_RECORD);
    });
});
