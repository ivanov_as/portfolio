import Immutable from 'immutable';
import chai from "chai";
import chaiImmutable from "chai-immutable";
import educationsReducer from '../../reducers/educations/educations';
import * as educationsAction from '../../actions/educations-action';
import {Education} from '../../constants/action-types';

let stateInitial;

const expect = chai.expect;

const EDUCATION_RECORD = Immutable.Map({id: 4, institute: 'Education 4', pulpit: 'pulpit 4', year: 2018});

const EDUCATION_RECORD_MODIFY = Immutable.Map({id: 1, institute: 'Education 11', pulpit: 'pulpit 1', year: 2018});

const EDUCATION_RECORD_REMOVE = Immutable.Map({id: 2, institute: 'Education 22', pulpit: 'pulpit 2', year: 2018});

chai.use(chaiImmutable);

describe('reducer educations', () => {
    beforeEach(() => {
        stateInitial = Immutable.fromJS([
            {id: 1, institute: 'Education 1', pulpit: 'pulpit 1', year: 2018},
            {id: 2, institute: 'Education 2', pulpit: 'pulpit 2', year: 2018},
            {id: 3, institute: 'Education 3', pulpit: 'pulpit 3', year: 2018}
        ]);
    });
    it(`reducer on action ${Education.FETCH_SUCCESS}`, () => {
        expect(educationsReducer(stateInitial, educationsAction.listEducationSuccess(stateInitial))).to.deep.equal(stateInitial);
    });
    it(`reducer on action ${Education.FETCH_FAILURE}`, () => {
        expect(educationsReducer(stateInitial, educationsAction.listEducationFailure)).to.deep.equal(stateInitial);
    });
    it(`reducer on action ${Education.CREATE_SUCCESS}`, () => {
        expect(educationsReducer(stateInitial, educationsAction.createEducationSuccess(EDUCATION_RECORD))).to.deep.equal(stateInitial.push(EDUCATION_RECORD));
    });
    it(`reducer on action ${Education.UPDATE_SUCCESS}`, () => {
        expect(educationsReducer(stateInitial, educationsAction.updateEducationSuccess(EDUCATION_RECORD_MODIFY))).to.deep.equal(stateInitial.set(0, EDUCATION_RECORD_MODIFY));
    });
    it(`reducer on action ${Education.UPDATE_FAILURE}`, () => {
        expect(educationsReducer(stateInitial, educationsAction.updateEducationFailure)).to.deep.equal(stateInitial);
    });
    it(`reducer on action ${Education.REMOVE_SUCCESS}`, () => {
        expect(educationsReducer(stateInitial, educationsAction.removeEducationSuccess(EDUCATION_RECORD_REMOVE))).to.deep.equal(stateInitial.delete(1));
    });
    it(`reducer on action ${Education.REMOVE_FAILURE}`, () => {
        expect(educationsReducer(stateInitial, educationsAction.removeEducationFailure)).to.deep.equal(stateInitial);
    });
});
