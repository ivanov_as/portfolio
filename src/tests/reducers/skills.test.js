import skillsReducer from '../../reducers/skills/skills';
import * as skillsAction from '../../actions/skills-action';
import {Skill} from '../../constants/action-types';
import Immutable from 'immutable';
import chai from "chai";
import chaiImmutable from "chai-immutable";

let stateInitial;

const SKILL_RECORD = Immutable.Map({id: 4, skill_name: 'Skill 4', skill_percent: 1});

const SKILL_RECORD_MODIFY = Immutable.Map({id: 1, skill_name: 'Skill edit', skill_percent: 1});

const SKILL_RECORD_REMOVE = Immutable.Map({id: 2, skill_name: 'Skill 2', skill_percent: 1});

const expect = chai.expect;

chai.use(chaiImmutable);

describe('reducer skills', () => {
    beforeEach(() => {
        stateInitial = Immutable.fromJS([
            {id: 1, skill_name: 'Skill 1', skill_percent: 1},
            {id: 2, skill_name: 'Skill 2', skill_percent: 1},
            {id: 3, skill_name: 'Skill 3', skill_percent: 1}
        ]);
    });
    it(`reducer on action ${Skill.FETCH_SUCCESS}`, () => {
        expect(skillsReducer(stateInitial, skillsAction.listSkillSuccess(stateInitial))).to.deep.equal(stateInitial);
    });
    it(`reducer on action ${Skill.FETCH_FAILURE}`, () => {
        expect(skillsReducer(stateInitial, skillsAction.listSkillFailure)).to.deep.equal(stateInitial);
    });
    it(`reducer on action ${Skill.CREATE_SUCCESS}`, () => {
        expect(skillsReducer(stateInitial, skillsAction.createSkillSuccess(SKILL_RECORD))).to.deep.equal(stateInitial.push(SKILL_RECORD));
    });
    it(`reducer on action ${Skill.UPDATE_SUCCESS}`, () => {
        expect(skillsReducer(stateInitial, skillsAction.updateSkillSuccess(SKILL_RECORD_MODIFY))).to.deep.equal(stateInitial.set(0, SKILL_RECORD_MODIFY));
    });
    it(`reducer on action ${Skill.UPDATE_FAILURE}`, () => {
        expect(skillsReducer(stateInitial, skillsAction.updateSkillFailure)).to.deep.equal(stateInitial);
    });
    it(`reducer on action ${Skill.REMOVE_SUCCESS}`, () => {
        expect(skillsReducer(stateInitial, skillsAction.removeSkillSuccess(SKILL_RECORD_REMOVE))).to.deep.equal(stateInitial.delete(1));
    });
    it(`reducer on action ${Skill.REMOVE_FAILURE}`, () => {
        expect(skillsReducer(stateInitial, skillsAction.removeSkillFailure)).to.deep.equal(stateInitial);
    });
});
