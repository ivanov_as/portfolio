import worksReducer from '../../reducers/works/works';
import * as worksAction from '../../actions/works-action';
import {Work} from '../../constants/action-types';
import Immutable from 'immutable';
import chai from "chai";
import chaiImmutable from "chai-immutable";

let stateInitial;

const WORK_RECORD = Immutable.Map({id: 4, work_name: 'Work 4'});

const WORK_RECORD_MODIFY = Immutable.Map({id: 1, work_name: 'Work edit'});

const WORK_RECORD_REMOVE = Immutable.Map({id: 2, work_name: 'Work 2'});

const expect = chai.expect;

chai.use(chaiImmutable);

describe('reducer works', () => {
    beforeEach(() => {
        stateInitial = Immutable.fromJS([
            {id: 1, work_name: 'Work 1'},
            {id: 2, work_name: 'Work 2'},
            {id: 3, work_name: 'Work 3'}
        ]);
    });
    it(`reducer on action ${Work.FETCH_SUCCESS}`, () => {
        expect(worksReducer(stateInitial, worksAction.listWorkSuccess(stateInitial))).to.deep.equal(stateInitial);
    });
    it(`reducer on action ${Work.FETCH_FAILURE}`, () => {
        expect(worksReducer(stateInitial, worksAction.listWorkFailure)).to.deep.equal(stateInitial);
    });
    it(`reducer on action ${Work.CREATE_SUCCESS}`, () => {
        expect(worksReducer(stateInitial, worksAction.createWorkSuccess(WORK_RECORD))).to.deep.equal(stateInitial.push(WORK_RECORD));
    });
    it(`reducer on action ${Work.UPDATE_SUCCESS}`, () => {
        expect(worksReducer(stateInitial, worksAction.updateWorkSuccess(WORK_RECORD_MODIFY))).to.deep.equal(stateInitial.set(0, WORK_RECORD_MODIFY));
    });
    it(`reducer on action ${Work.UPDATE_FAILURE}`, () => {
        expect(worksReducer(stateInitial, worksAction.updateWorkFailure)).to.deep.equal(stateInitial);
    });
    it(`reducer on action ${Work.REMOVE_SUCCESS}`, () => {
        expect(worksReducer(stateInitial, worksAction.removeWorkSuccess(WORK_RECORD_REMOVE))).to.deep.equal(stateInitial.delete(1));
    });
    it(`reducer on action ${Work.REMOVE_FAILURE}`, () => {
        expect(worksReducer(stateInitial, worksAction.removeWorkFailure)).to.deep.equal(stateInitial);
    });
});
