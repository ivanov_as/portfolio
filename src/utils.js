function splitListByField(List, Field) {
    let result = '';

    List.map((item, key) => {
        result += item.get(Field) + ((key + 1 < List.size) ? ', ' : '');
        return item;
    });
    return result;
}

function splitAssociateListByField(List, Associate, Field) {
    let result = '';

    List.map((item, key) => {
        result += item.get(Associate).get(Field) + ((key + 1 < List.size) ? ', ' : '');
        return item;
    });
    return result;
}

function decimal(num = 0) {
    return num - Math.floor(num);
}

const Utils = {
    splitListByField,
    splitAssociateListByField,
    decimal
};

export default Utils;